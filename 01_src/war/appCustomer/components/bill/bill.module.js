var billMod = angular.module('billMod', [])

billMod.controller('listBillsCtrl', function($scope, $routeParams, deviceService) {
	$scope.entries = {
			bills: []
	};
	
	$scope.listBills = function() {
		var data = {
				id: $routeParams.id
		}
		
		deviceService.getDevice(data, $scope);
	}
	
	$scope.sort = function(keyName) {
		$scope.sortKey = keyName;
		$scope.reverse = !$scope.reverse
		console.log($scope.reverse)
	}
	
	$scope.listBills();
})

billMod.controller('listDeletedBillCtrl', function($scope, $localStorage) {
	$scope.entries = {
			deletedBills: []
	};
	
	$scope.listDeletedBills = function() {
		$scope.entries.deletedBills = $localStorage.user.deletedBills;
	}
	
	//$scope.listDeletedBills();
})