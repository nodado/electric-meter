loginMod.service('loginService', function($http, $localStorage, $location) {
	this.submit = function(data, scope) {
		var url = '/LoginCustomer';
		var headers = {
				headers: {'Content-Type': 'application/json'}
		};
		
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							$localStorage.user = response.data.customer;
							$location.path('/MyDevices');
							$localStorage.login = true;
							scope.$parent.login = true;
							scope.$parent.name = $localStorage.user.firstName + " " + $localStorage.user.lastName;
							console.log($localStorage.user.devices);
						}else {
							alert(response.data.errorList);
						}
					}
			)
	}
})