var loginMod = angular.module('loginMod', [])

loginMod.controller('loginCtrl', function($scope, $location, $localStorage,loginService) {
	$scope.user = {
			username: "",
			password: ""
	}
	$scope.submit = function() {
		var user = new $scope.User();
		loginService.submit(user, $scope);
	}
	
	$scope.User = function() {
		this.username = $scope.user.username;
		this.password = $scope.user.password;
	}
})