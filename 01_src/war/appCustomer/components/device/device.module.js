var deviceMod = angular.module('deviceMod', [])

deviceMod.controller('listDeviceCtrl', function($scope, $localStorage, $location, $routeParams, deviceService) {
	$scope.entries = {
			devices: []
	};
	
	
	$scope.billStatus = ["Paid", "Unpaid"];
	
	$scope.bill = {
			billStatus: $scope.billStatus[0]
	}
	$scope.listDevices = function() {
		var devices = {
				devices: []
		}
		var userId = {
				id: $localStorage.user.id
		}
		deviceService.getDevicesById(userId, $scope);
		$localStorage.user.myDevices = $scope.entries;
		console.log($localStorage.user.myDevices )
	}
	
	$scope.onStatus = function() {
		var device;
		if($scope.bill.billStatus == "Unpaid") {
			device = {
					customerId: $localStorage.user.id,
					status: false
			}
		}else {
			device = {
					customerId: $localStorage.user.id,
					status: true
			}
		}
		deviceService.getDeviceByStatus(device, $scope);
	}
	
	$scope.editDevice = function(device) {
		$location.path('/MyDevices/' + device.id)
	}
	
	$scope.sort = function(keyName) {
		$scope.sortKey = keyName;
		$scope.reverse = !$scope.reverse
		console.log($scope.reverse)
	}
	
	$scope.listDevices();
	$scope.onStatus();
})
