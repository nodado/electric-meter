deviceMod.service('deviceService', function($http, $httpParamSerializer) {

	this.getMyDevices = function(data, scope) {
		var url = '/GetCustomerDevices';
		var headers = {
				headers:  {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response=> {
						scope.entries.devices = response.data.devices;
						if(scope.entries.devices !== undefined) {
							for(d of scope.entries.devices){
									var date = new Date(d.installationDate);
									var installDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
									d.installationDate = installDate;
							}
						}
					},
					error => {
						
					}
			)
	}
	
	this.getMyDevicesPendingBill = function(data, scope) {
		var url = '/GetDevicesPendingBill';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						var devices = response.data.devices;
						if(devices.length > 0) {
							scope.entries.devicesPendingBill = devices;
							scope.device.deviceWithBill = scope.entries.devicesPendingBill[0];
							this.getDeviceUnpaidBill(scope.device.deviceWithBill, scope);
							scope.hasBill = true;
							scope.noBill = false;
						}else {
							scope.hasBill = false;
							scope.noBill = true;
						}
					},
					error => {
						
					}
			)
	}
	
	this.getDeviceByStatus = function(device, scope) {
		var url = "/GetCustomerDeviceByStatus";
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		}
		$http.post(url, $httpParamSerializer(device), headers)
			.then(
				response => {
					scope.entries.devices  = response.data.devices;
					console.log(scope.entries.devices )
				},
				error => {
					
				}
			)
	}
	
	this.getDevicesById = function(data, scope) {
		var url = '/GetCustomerDevicesIds';
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		}
		$http.post(url, $httpParamSerializer(data), headers)
		.then(
			response => {
				var devices = {
						devices: response.data.devicesIds.devicesIds
				}
				if(devices.devices.length > 0){
					this.getMyDevices(devices, scope);
				}
			},
			error => {
				alert('error in connecting to server.');
			}
		)
	}
	
	this.getDevice = function(data, scope) {
		var url = '/GetDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		};
		$http.post(url, $httpParamSerializer(data), headers)
			.then(
				response => {
					var device = response.data.device.billsIds;
					if(device !== undefined) {
						this.getDeviceBills(response.data.device.billsIds, scope);
					}
				},
				error => {
					alert('error in connecting to server.');
				}
			)
	}
	
	
	this.getDeviceUnpaidBill = function(data, scope) {
		var url = "/GetUnpaidBill";
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		var billsId = {
				billsId: data.billId
		}
		$http.post(url, billsId, headers)
			.then(
					response => {
						var unpaidBill = response.data.unpaidBill;
						if(unpaidBill !== undefined) {
							var date = new Date(response.data.unpaidBill.readingDate);
							unpaidBill.readingDate = {
									month: date.getMonth() + 1,
									day: date.getDate(),
									year: date.getFullYear()
							}
							scope.device.deviceWithBill.billId = unpaidBill.id;
							scope.device.deviceWithBill.reading = unpaidBill.reading;
							scope.device.deviceWithBill.readingDate = unpaidBill.readingDate;
							scope.device.deviceWithBill.amount = unpaidBill.amount;				
						}
					}
			)
	}
	
	this.getDeviceBills = function(data, scope) {
		var url = "/GetBills";
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		var bills = {
				billsIds: data
		}
		$http.post(url, bills, headers)
		.then(
			response => {
				scope.entries.bills = response.data.bills;
				if(scope.entries.bills !== undefined) {
					for(d of scope.entries.bills){
						var date = new Date(d.readingDate);
	
						var installDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
						d.readingDate = installDate;
				}
			}
			},
			error => {
				alert('error in connecting to server.');
			}
		)
	}
	
	this.payBill = function(data) {
		var url = '/UpdateBill';
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		}
		$http.post(url, $httpParamSerializer(data), headers)
			.then(
					response => {
						
					}, 
					error =>{
						
					}
			)
	}
	
	this.updateDevicePendingBill = function(device) {
		var url = '/UpdateDevicePendingBill';
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		}
		$http.post(url, $httpParamSerializer(device), headers) 
			.then(
					response => {
						
					},
					error => {
						
					}
			)
	}
})
