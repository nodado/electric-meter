var app = angular.module('app', ['ngRoute', 'ngStorage','deviceMod','billMod', 'loginMod'])

app.controller('mainCtrl', function($scope, $location, $localStorage, deviceService){
	$scope.photo = "../assets/no_user.jpg";
	$scope.name = "Kevin Monteclar";
	$scope.login = false;
	$scope.readingRate = "10.8584/kwh";
	
	$scope.entries = {
			devicesPendingBill: [],
			devices: []
	}
	
	$scope.device = {
			deviceWithBill: {}
	}
	
	$scope.amount;
	
	$scope.logout = function() {
		$localStorage.login = false;
		$scope.login = false;
		$location.path('/');
		$localStorage.user = "";
	}
	
	$scope.isLogin = function() {
		if($localStorage.login) {
			$scope.login = true;
			//$scope.hasPhoto();
			$scope.name = $localStorage.user.username;
		}else {
		}
	}
	
	$scope.listDevicesWithBills = function() {
		var devices = $localStorage.user.devices;
		var data = {
				devices: devices
		}
		deviceService.getMyDevicesPendingBill(data, $scope);
	}
	
	$scope.getDeviceUnpaidBill = function() {
		deviceService.getDeviceUnpaidBill($scope.device.deviceWithBill, $scope);
	}
	
	$scope.changeDevice = function() {
		$scope.getDeviceUnpaidBill();
	}
	
	$scope.pay = function() {
		if($scope.amount < $scope.device.deviceWithBill.amount) {
			$scope.invalidAmount = true;
		}else {
			var bill = {
					id: $scope.device.deviceWithBill.billId,
					status: true
			}
			var device = {
					id: $scope.device.deviceWithBill.id,
					pendingBill: false
			}
			deviceService.payBill(bill);
			deviceService.updateDevicePendingBill(device);
			deviceService.getDevice(device, $scope);
		}
	}
 	
	$scope.showModal = function() {
		var modalWrapper = angular.element(document.querySelector('#payModalWrapper'));
		var modalContent = angular.element(document.querySelector('#modal'));
		var close = angular.element(document.querySelector('#close'));
		
		$scope.listDevicesWithBills();
		modalWrapper.addClass('showModalWrap');
		modalContent.addClass('showModalContent');
		
		close.on('click', function() {
				modalWrapper.removeClass('showModalWrap');
				modalContent.removeClass('showModalContent');
		})
		
	}
	
	$scope.changeLocation = function(pageLink) {
		$location.path(pageLink);
	}
	
	$scope.isLogin();
	
})
