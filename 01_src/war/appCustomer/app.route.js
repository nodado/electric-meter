app.config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/', {
			resolve: {
				'check': function($location, $localStorage) {
					if($localStorage.login) {
						$location.path('/MyDevices');
					}
				}
			},
			templateUrl: '/appCustomer/components/login/Login.html',
			controller: 'loginCtrl'
		})
		.when('/Notification', {
			resolve: {
				'check': function($location, $localStorage) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appCustomer/components/notification/Notification.html'
		})
		.when('/MyDevices', {		
			resolve: {
				'check': function($location, $localStorage) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appCustomer/components/device/Device.html',
			controller: 'listDeviceCtrl'
		})
		.when('/MyDevices/:id', {			
			resolve: {
				'check': function($location, $localStorage) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appCustomer/components/bill/Bills.html',
			controller: 'listBillsCtrl'
		})
		.when('/DeletedBills', {		
			resolve: {
				'check': function($location, $localStorage) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appCustomer/components/bill/DeletedBills.html',
			controller: 'listDeletedBillCtrl'
		})
		.when('/Profile', {
			
		})
}])