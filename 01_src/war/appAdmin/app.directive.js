app.directive('sidebar', function($timeout) {
	var sidebarFn;
	var linkFn;
	var slide = true;
	
	$timeout(linkFn, 150);
	
	
	linkFn = function(scope, element, attr) {
		var sidebar = angular.element(document.querySelector('#sidebar'));
		scope.menu.dashboard = angular.element(document.querySelector('#dashboard'));
		scope.menu.customer.dom = angular.element(document.querySelector('#customer'));
		scope.menu.customer.subMenu.dom = angular.element(document.querySelector('#subCustomer'));
		scope.menu.device.dom = angular.element(document.querySelector('#device'));
		scope.menu.device.subMenu.dom = angular.element(document.querySelector('#subDevice'));
		scope.menu.admin.dom = angular.element(document.querySelector('#admin'));
		scope.menu.admin.subMenu.dom = angular.element(document.querySelector('#subAdmin'));
				
		scope.menu.sidebarFn = function(category) {
			category.dom.on('click', function() {
					if(category.subMenu.hide) {
						category.subMenu.dom.addClass("show");
						category.subMenu.hide = false;
					}else {
						category.subMenu.dom.removeClass("show");
						category.subMenu.hide = true;
					}
			});
		}
		
//		scope.menu.checkSidebarFn = function() {
//			if(angular.element(document.querySelector('#sidebar')).prop('offsetWidth') === 60){
//				scope.sidebarMode.tab = true;
//				scope.sidebarMode.mobile = scope.sidebarMode.desktop = false;
//			}else if(angular.element(document.querySelector('#sidebar')).prop('offsetWidth') === 200) {
//				scope.sidebarMode.desktop = true;
//				scope.sidebarMode.mobile = scope.sidebarMode.tab = false;
//			}else{
//				scope.sidebarMode.mobile = true;
//				scope.sidebarMode.desktop = scope.sidebarMode.tab = false;
//			}
//			console.log("mobileView: " + scope.sidebarMode.mobile);
//			console.log("tabView: " + scope.sidebarMode.tab);
//			console.log("desktopView: " + scope.sidebarMode.desktop);
//			console.log(angular.element(document.querySelector('#sidebar')).prop('offsetWidth'));
//			}
//		
//		scope.sidebarMode.down = function() {
//			 if(!scope.sidebarMode.isDown) {
//				 angular.element(document.querySelector('#sidebar')).addClass("showSidebar");
//				 scope.sidebarMode.isDown = true;
//			 }else {
//				 angular.element(document.querySelector('#sidebar')).removeClass("showSidebar");
//				 scope.sidebarMode.isDown = false;
//			 }
//		 }
//	
//		scope.sidebarMode.slide = function() {
//			angular.element(document.querySelector('#sidebar')).addClass("fullSidebar");
//			angular.element(document.querySelector('#logo')).addClass("fullLogo");
//			angular.element(document.querySelector('#barContainer')).addClass("fullBarContainer");
//			angular.element(document.querySelector('#sidebar')).removeClass("hideSidebar");
//			scope.menu.checkSidebarFn();
//		 }
//		
//		scope.sidebarMode.hide = function() {
//			angular.element(document.querySelector('#sidebar')).removeClass("fullSidebar");
//			angular.element(document.querySelector('#logo')).removeClass("fullLogo");
//			angular.element(document.querySelector('#barContainer')).removeClass("fullBarContainer");
//			angular.element(document.querySelector('#sidebar')).addClass("hideSidebar");
//			angular.element(document.querySelector('#sidebar')).addClass("hideSidebar");
//			scope.menu.checkSidebarFn();
//		}
//		
//		window.onresize = function() {
//			scope.menu.checkSidebarFn();
//		}
//		
//		scope.menu.checkSidebarFn();
//		
		scope.menu.sidebarFn(scope.menu.customer);
		scope.menu.sidebarFn(scope.menu.device);
		scope.menu.sidebarFn(scope.menu.admin);
	}
	return {
		link: linkFn
	}
})


app.directive('headerOption', function($timeout) {
	var linkFn;
	var bar;
	var hide = true;
	
	$timeout(linkFn, 500);
	
	linkFn = function(scope, element, attr) {
		bar = angular.element(document.querySelector('#barContainer'));
		var sidebar = angular.element(document.querySelector('#sidebar'));
		
		bar.on('click', function() {
			scope.menu.checkSidebarFn();
			if(scope.sidebarMode.mobile) {
				scope.sidebarMode.down();
			}else if(scope.sidebarMode.tab) {
				scope.sidebarMode.slide();
			}else {
				scope.sidebarMode.hide();
			}
		})
		
		
	}
	
	return {
		link: linkFn
	}
})

app.directive('showModal', function($rootScope) {
	var linkFn;
	linkFn = function(scope, element, attr) {
		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
		var modal = angular.element(document.querySelector('#modal'));
		var addBill = angular.element(document.querySelector('#addBill'));
		addBill.on('click', function() {
			var pay = scope.canAdd();
			if(!scope.canAdd()) {
				modalWrapper.addClass('showModal');
				modal.addClass('showModalContent');
			}

		})
	}

	return {
		link: linkFn
	}
})

app.directive('showDeviceModal', function() {
	var linkFn;
	linkFn = function(scope, element, attr) {
		var modalWrapper = angular.element(document.querySelector('#modalDeviceWrapper'));
		var modal = angular.element(document.querySelector('#modalDevice'));
		var addBill = angular.element(document.querySelector('#addDevice'));
		addBill.on('click', function() {
			modalWrapper.addClass('showModalWrap');
			modal.addClass('showModalContent');
		})
	}
	return {
		link: linkFn
	}
})


app.directive('close', function($timeout) {
	var linkFn;
	linkFn = function(scope, element, attr) {
		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
		var modal = angular.element(document.querySelector('#modal'));
		var close = angular.element(document.querySelector('#close'));
		
		close.on('click', function() {

			modal.removeClass('showModalContent');
			$timeout(function() {
				modalWrapper.removeClass('showModal');
			}, 290)
		})
	}
	return {
		link: linkFn
	}
})

app.directive('closeDevice', function($timeout) {
	var linkFn;
	linkFn = function(scope, element, attr) {
		var modalWrapper = angular.element(document.querySelector('#modalDeviceWrapper'));
		var modal = angular.element(document.querySelector('#modalDevice'));
		var close = angular.element(document.querySelector('#closeDevice'));
		
		close.on('click', function() {
			modal.removeClass('showModalContent');
			$timeout(function() {
				modalWrapper.removeClass('showModalWrap');
			}, 290)
		})
	}
	return {
		link: linkFn
	}
})