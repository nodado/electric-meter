app.config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/', {
			resolve: {
				'check': function($localStorage, $location) {
					if($localStorage.login) {
						$location.path('/Dashboard');
					}
				}
			},
			templateUrl: '/appAdmin/components/login/Login.html',
			controller: 'loginCtrl'
		})
		.when('/Dashboard', {
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/dashboard/Dashboard.html'
		})
		.when('/ListCustomer', {
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/customer/ListCustomers.html',
			controller: "listCustomerCtrl"
		})
		.when('/ListDevice', {
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/device/ListDevice.html',
			controller: "listDeviceCtrl"
		})
		.when('/ListAdmin', {	
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/admin/ListAdmin.html',
			controller: "listAdminCtrl"
		})
		.when('/CustomerDetails', {		
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/customer/CustomerDetails.html',
			controller: 'customerDetailsCtrl'
		})
		
		.when('/AdminDetails', {
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/admin/AdminDetails.html',
			controller: 'adminDetailsCtrl'
		})
		.when('/DeviceDetails/:id', {			
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/device/DeviceDetails.html',
			controller: 'deviceDetailsCtrl'
		})
		.when('/EditAdminDetails/:id', {			
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/admin/AdminDetails.html',
			controller: 'editAdminDetailsCtrl'
		})
		.when('/EditCustomerDetails/:id', {			
			resolve: {
				'check': function($localStorage, $location) {
					if(!$localStorage.login) {
						$location.path('/');
					}
				}
			},
			templateUrl: '/appAdmin/components/customer/CustomerDetails.html',
			controller: 'editCustomerDetailsCtrl'
		})
}])