var app = angular.module('app', ['deviceMod', 'loginMod','customerMod', 'adminMod', 'ngStorage','ngRoute'])

app.controller('mainCtrl', function($scope, $location, $localStorage){
	$scope.name;
	$scope.login = false;
	$scope.photo = "../assets/no_user.jpg";
	$scope.gender = [
	                 	"Select a gender",
	                 	"Male",
	                 	"Female",
	                 	"Other"
	                 ]
	$scope.months = [
			 {id: 0, value: "Month"},
             {id: 1, value:"January"},
             {id: 2, value:"February"},
             {id: 3, value:"March"},
             {id: 4, value:"April"},
             {id: 5, value:"May"},
             {id: 6, value:"June"},
             {id: 7, value:"July"},
             {id: 8, value:"August"},
             {id: 9, value:"September"},
             {id: 10, value:"October"},
             {id: 11, value:"November"},
             {id: 12, value:"December"},
	]	
	$scope.days = ["Day"];	
	$scope.years = ["Year"];
	
	$scope.initDate= function() {
		for(var i = 1; i <= 31; i++) {
			$scope.days.push(i);
		}
		
		for(var i = 2016; i >= 1975; i--) {
			$scope.years.push(i);
		}
	}
	
	$scope.sidebarMode = {
			mobile: false,
			tab: false,
			desktop: false,
			down: "",
			show: "",
			hide: "",
			isSlide: false,
			isDown: false,
			isHide: false
	}
	$scope.menu = {
			dashboard: "",
			customer: {
				dom: "",
				subMenu: {
					dom: "",
					customers: "",
					addCustomer: "",
					hide: true
				}
			},
			device: {
				dom: "",
				subMenu: {
					dom: "",
					devices: "",
					addDevice: "",
					hide: true
				}
			},
			admin: {
				dom: "",
				subMenu: {
					dom: "",
					admins: "",
					addAdmin: "",
					hide: true
				}
			},
			bills: "",
			logout: "",
			sidebarFn: "",
			checkSidebarFn: ""
		}
	
	$scope.changeLocation = function(pageLink) {
		$location.path(pageLink);
	}
	
	$scope.hasPhoto = function() {
		if($localStorage.user.photo === undefined) {
			$scope.photo = "../assets/no_user.jpg";
		}else {
		}
	}
	
	$scope.isLogin = function() {
		if($localStorage.login) {
			$scope.login = true;
			//$scope.hasPhoto();
			$scope.name = $localStorage.user.username;
			console.log($localStorage.user);
		}else {
		}
	}
	
	$scope.logout = function() {
		$localStorage.user = "";
		$localStorage.login = false;
		$location.path('/');
		$scope.login = false;
	}
	
	$scope.isLogin();
	$scope.initDate();
	
})