var adminMod = angular.module('adminMod', [])

adminMod.controller('listAdminCtrl', function($scope, $location, adminService) {
	$scope.entries = {
		admins: []	
	}
	
	$scope.listAdmins = function() {
		adminService.getAdmins($scope.entries);
	}
		
	$scope.editAdmin = function(admin) {
		$location.path('/EditAdminDetails/' + admin.id);
	}
	
	$scope.deleteAdmin = function(admin) {
		adminService.deleteAdmin(admin, $scope);
	}
	
	$scope.sort = function(keyName) {
		$scope.sortKey = keyName;
		$scope.reverse = !$scope.reverse
		console.log($scope.reverse)
	}
	
	$scope.listAdmins(); 
})

adminMod.controller('adminDetailsCtrl', function($scope, adminService) {
	
	$scope.disable = false;
	
	$scope.admin = {
			firstName: "",
			lastName: "",
			gender:  $scope.$parent.gender[0],
			birthday: {
				month:  $scope.$parent.months[0],
				day:  $scope.$parent.days[0],
				year: $scope.$parent.years[0]
			},
			email: "",
			username: "",
			password: "",
			rights: {
				canAdd: false,
				canEdit: false,
				canDelete: false,
				canRights: false
			}
	}

	$scope.addAdmin = function() {
		var admin = new $scope.Admin();
		adminService.addAdmin(admin, $scope);
	}
	
	$scope.Admin = function() {
		this.firstName = $scope.admin.firstName;
		this.lastName = $scope.admin.lastName;
		this.gender = $scope.admin.gender;
		this.birthday = {
				month: $scope.admin.birthday.month.id,
				day: $scope.admin.birthday.day,
				year: $scope.admin.birthday.year
		}
		this.email = $scope.admin.email;
		this.username = $scope.admin.username;
		this.password = $scope.admin.password;
		this.rights = $scope.admin.rights;
		
	}
	
	$scope.clear = function() {
		$scope.admin.firstName = "";
		$scope.admin.lastName = "";
		$scope.admin.gender = $scope.$parent.gender[0],
		$scope.admin.birthday = {
			month:  $scope.$parent.months[0],
			day:  $scope.$parent.days[0],
			year: $scope.$parent.years[0]
		}
		$scope.admin.email = "";
		$scope.admin.username = "";
		$scope.admin.password = "";
		$scope.admin.rights.canAdd = false;
		$scope.admin.rights.canEdit = false;
		$scope.admin.rights.canDelete = false;
		$scope.admin.rights.canRights = false;
	}

})

adminMod.controller('editAdminDetailsCtrl', function($scope, $routeParams, adminService) {
	$scope.admin = {
			firstName: "",
			lastName: "",
			gender: "",
			birthday: {
				month: "",
				day: "",
				year: ""
			},
			email: "",
			username: "",
			password: "",
			rights: {
				canAdd: false,
				canEdit: false,
				canDelete: false,
				canRights: false
			}
	}
	
	$scope.disable = true;
	
	$scope.getAdmin = function() {
		var admin = {
				id: $routeParams.id
		}
		adminService.getAdmin(admin, $scope);

	}
	
	$scope.addAdmin = function() {
		var admin = new $scope.Admin();
		adminService.editAdmin(admin);
	}
	
	$scope.Admin = function() {
		this.id = $scope.admin.id;
		this.firstName = $scope.admin.firstName;
		this.lastName = $scope.admin.lastName;
		this.gender = $scope.admin.gender;
		this.birthday = $scope.admin.birthday;
		this.email = $scope.admin.email;
		this.username = $scope.admin.username;
		this.password = $scope.admin.password;
		this.rights = $scope.admin.rights;
	}
	
	$scope.clear = function() {
		$scope.admin.firstName = "";
		$scope.admin.lastName = "";
		$scope.admin.gender = "Select gender";
		$scope.admin.birthday.month = "Month";
		$scope.admin.birthday.day = "Day";
		$scope.admin.birthday.year = "Year";
		$scope.admin.email = "";
		$scope.admin.username = "";
		$scope.admin.password = "";
		$scope.admin.rights.canAdd = false;
		$scope.admin.rights.canEdit = false;
		$scope.admin.rights.canDelete = false;
		$scope.admin.rights.canRights = false;
	}

	$scope.getAdmin();
})
