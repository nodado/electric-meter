adminMod.service('adminService', function($http, $httpParamSerializer) {
	this.getAdmins = function(entries) {
		var url = '/ListAdmin';
		$http.get(url)
			.then(
					response => {
						entries.admins = response.data.adminList;
						console.log(response.data.adminList)
					}
			)
	}
	
	this.getAdmin = function(data, scope) {
		var url = '/GetAdmin';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		};
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							var admin = response.data.admin;
							var ctr = 0;
							var date = new Date(admin.birthday);
							var month = (date.getMonth() + 1);
							var day = date.getDate();
							var year = date.getFullYear();
							
							
							scope.admin.id = admin.id;
							scope.admin.firstName = admin.firstName;
							scope.admin.lastName = admin.lastName;
							scope.admin.gender = admin.gender;
							scope.admin.email = admin.email;
							scope.admin.username = admin.username;
							
							  for(m of scope.$parent.months) {
							    	if(month == m.id) {
							    	    scope.admin.birthday.month = m.id;
							    	    break;
							    	}
							    }
							    
							    for(d of scope.$parent.days) {
							    	if(day == d) {
							    		scope.admin.birthday.day = d;
							    		break;
							    	}
							    }
							    
							    for(y of scope.$parent.years) {
							    	if(year == y) {
							    		scope.admin.birthday.year = y;
							    		break;
							    	}
							    }
							
							for(r in scope.admin.rights) {
								scope.admin.rights[r] = admin.rights[ctr++];
							}
							
							console.log(scope.admin);
							
						}else {
							
						}
					},
					error => {
						
					}
			)
	}
	
	this.addAdmin = function(data, scope) {
		var url = '/RegisterAdmin';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							alert('add successful');
							scope.clear();
						}else {
							alert('add error')
						}
					}
			)
		return false;
	}
	
	this.editAdmin = function(data) {
		var url = '/UpdateAdmin';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							alert('update successful')
						}else {
							alert('update error')
						}
					}
			)
	}
	
	this.deleteAdmin = function(data, scope) {
		var url = '/DeleteAdmin';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
							var index = scope.entries.admins.indexOf(data);
							scope.entries.admins.splice(index, 1);
							alert('delete successful');
					}
			)
			return false;
	}
})