deviceMod.service('deviceService', function($http, $httpParamSerializer) {
	var devices;
	var self = this;
	
	this.getDevices = function(entries) {
		var url = '/ListDevices';
		$http.get(url)
			.then(
				response => {
					if(response.data.errorList.length === 0) {
						entries.devices = response.data.deviceList;
						if(entries.devices.length > 0) {
							for(d of entries.devices) {
								if(d.installationDate !== undefined){
									var date = new Date(d.installationDate);
	
									var installDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
									console.log(installDate)
									d.installationDate = installDate;
								}
							}
							console.log(response.data.deviceList)
						}
					}
				}
			)
	}
	
	this.getDevicesByInstallationDate = function(data, scope) {
		var url = '/ListDevicesByInstallationDate';
		var headers = {
				'Content-Wrapper': 'application/json'
		};
		$http.post(url, data, headers)
			.then(
					response => {
						console.log(response.data.devices);
					},
					error => {
						
					}
			)
	}
	
	this.getDevicesByStatus = function(data, scope) {
		var url = "/GetDevicesByStatus";
		var headers ={
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		}
		$http.post(url, $httpParamSerializer(data), headers)
			.then(
					response => {
						var devices = response.data.devices;
						if(devices.length > 0) {
							for(d of devices) {
								if(d.installationDate !== undefined){
									var date = new Date(d.installationDate);
	
									var installDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
									console.log(installDate)
									d.installationDate = installDate;
								}
							}
						}
						scope.entries.devices = devices;
					},
					error => {
					}
				)
	}
	
	this.getDevice = function(data, scope) {
		var url = '/GetDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		};
		$http.post(url, $httpParamSerializer(data), headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							var device = response.data.device;
						
							if(device !== undefined) {
								var ctr = 0;
								var date = new Date(device.installationDate);
								
								scope.device.id = device.id;
								scope.device.meterNumber = device.meterNumber;
								scope.device.installationDate = date;
		
								if(scope.device.installationDate !== undefined) {
									var month = date.getMonth() + 1;
									var day = date.getDate();
									var year = date.getFullYear();
									
									for(m of scope.$parent.months) {
										if(m.id == month) {
											scope.device.installation.month = m;
											break;
										}
									}
									
									scope.device.installation.year = year;
									scope.device.installation.day = day;
									
									scope.device.billsIds = device.billsIds;
									scope.device.owner = device.ownerId;
									scope.device.ownerId = device.ownderId;
									
									this.getDeviceOwner(scope.device);
									
									var bills = {
										billsIds: scope.device.billsIds
									}
									
									this.getDeviceBills(bills, scope);
									
									scope.notInstalled = true;
	
								}else {
									scope.device.owner = "none"
								}
							}
							
						}else {
							alert(response.data.errorList);
						}
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	
	this.getDeviceBills = function(data, scope) {
		var url = '/GetDeviceBills';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						scope.device.bills = response.data.bills;
						if(scope.device.bills.length > 0) {
							for(bill of scope.device.bills) {
								var date = new Date(bill.readingDate);
								bill.readingDate = {
										month: date.getMonth() + 1,
										day: date.getDate(),
										year: date.getFullYear()
								}
							}
							console.log(scope.device.bills)
						}
					},
					error => {
						alert('Error in connecting to server,')
					}
			)
	}
	
	this.getDeviceOwner = function(data) {
		var url = '/GetCustomerById';
		var headers = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		}
		var device = {
				id: data.ownerId
		};
		$http.post(url, $httpParamSerializer(device), headers)
		.then(
				response => {
					var owner = response.data.customer;
					data.owner = owner.firstName + " " + owner.lastName;
				}
		)
	}
	
	this.getDevicesNoOwner = function(entries, device) {
		var url = '/ListDevicesNoOwner';
		$http.get(url)
		.then(
				response => {
					entries.availableDevices = response.data.devices;
					if(entries.availableDevices.length === 0) {
						device.customerDevice = entries.availableDevices[0];
					}else {
						device.customerDevice = entries.availableDevices[0];
					}
					device.customerDevice = entries.availableDevices[0];
				},
				error => {
					alert('error in connecting to server.');
				}
		)
	}
	
	this.addDevice = function(data, scope) {
		var url = '/RegisterDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							alert('add successful');
							scope.clear();
						}else {
							alert('add error' + response.data.errorList);
						}
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	this.updateDeviceDate = function(data) {
		var url = "/UpdateDeviceDate";
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						
					},
					error => {
						
					}
			)
	}
	
	this.addBill = function(data, scope) {
		var url = '/CreateBill';
		var headers = {
				headers: {
					'Content-Type' : 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							var billId = response.data.bill;
							scope.bill.device.billId.push(billId.id);
							var data = {
								id: scope.bill.device.id,
								billsId: scope.bill.device.billId,
								pendingBill: true
							}
							this.registerBillToDevice(data);
						}else {
							alert('add error');
						}
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	this.updateBill = function(data) {
		var url = '/UpdateBillDetails';
		var headers = {
				'Content-Type': 'application/json'
		}
		$http.post(url, data, headers)
			.then(
					response => {
						alert('edit successful');
					},
					error => {
						alert('error in connecting to server.');
					}
			)
			
	}
	
	this.editDevice = function(data) {
		var url = '/UpdateDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							alert('update successful')
						}else {
							alert('update error')
						}
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	this.registerBillToDevice = function(data){
		var url = '/RegisterBillToDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							alert('update successful')
						}else {
							alert('update error')
						}
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	this.deleteDevice = function(data, entries) {
		var url = '/DeleteDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
							var index = entries.devices.indexOf(data);
							entries.devices.splice(index, 1);
							alert('delete successful');
							
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	this.deleteBills = function(data, scope) {
		var url = '/DeleteDeviceBills';
		var headers = {
				headers: {
					'ContentType' : 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
							alert('delete successful');
							var data = {
									id: scope.device.id,
									billsId: scope.device.billsIds
							};
							
							this.deleteBillsOfDevice(data, scope);
					
					},
					error => {
						alert('error in connecting to server.');
					}
			)
	}
	
	this.deleteBillsOfDevice = function(data, scope) {
		var url = '/DeleteBillsOfDevice';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
					},
					error => {
						alert('error in connectong to server')
					}
			)
	}
})