var deviceMod = angular.module('deviceMod', []);

deviceMod.controller('listDeviceCtrl', function($scope, $location, deviceService) {
	$scope.entries = {
			devices: [],
			filteredDevices: []
		};
	
	$scope.billStatus = ["Paid", "Unpaid"];
	
	$scope.billStatus.bill = $scope.billStatus[0];
	
	$scope.listDevice = function() {
		deviceService.getDevices($scope.entries);
	}
	
	$scope.installationDate = {
			from: {
				month: "",
				day: "",
				year: ""
			},
			to: {
				month: "",
				day: "",
				year: ""
			}
	}
	
	$scope.onStatus = function() {
		var status;
		if($scope.billStatus.bill === "Paid") {
			status = false;
		}else {
			status = true;
		}
		var deviceStatus = {
				status: status
		}
		deviceService.getDevicesByStatus(deviceStatus,$scope);
	}
	
	$scope.deleteDevice = function(device) {
		if(device.customerId === 0){
			deviceService.deleteDevice(device, $scope.entries);
		}
		else {
			alert("Can't delete a device that is installed to a customer.");
		}
	}
	
	$scope.editDevice = function(device) {
		$location.path('/DeviceDetails/' + device.id);
	}
	
	$scope.find = function() {
		$scope.entries.filteredDevices = [];
		$scope.filterByDate();
	}
	
	$scope.filterByDate = function() {
//		var fromDate = new Date($scope.installationDate.from.month.id + "/" + $scope.installationDate.from.day + "/" + $scope.installationDate.from.year);
//		var toDate = new Date($scope.installationDate.to.month.id + "/" + $scope.installationDate.to.day + "/" + $scope.installationDate.to.year);	
//		var month = device.installationDate.month;
//		var day = device.installationDate.day;
//		var year =  device.installationDate.year;
//		
//		deviceService.getDevicesByInstallationDate()
	}
	
	$scope.sort = function(keyName) {
		$scope.sortKey = keyName;
		$scope.reverse = !$scope.reverse
		console.log($scope.reverse)
	}
	
	$scope.listDevice();
	//$scope.onStatus();
})

deviceMod.controller('deviceDetailsCtrl', function($scope, $routeParams, $location, deviceService) {
	
	$scope.device = {
			id: "",
			installationDate: "",
			installation: {
				month: "",
				year: "",
				day: ""
			},
			meterNumber: "",
			owner: "",
			bills: [],
			billsIds: []
	}
	
	$scope.editBill = {
			id: "",
			reading: "",
			readingDate: {
				month: "",
				day: "",
				year: ""
			},
			amount: ""
	}
	
	$scope.toBeDeletedBills = [];

	$scopeunpaid = false;
	
	$scope.notInstalled = false;
	
	$scope.getDevice = function() {
		var device = {
				id: $routeParams.id
		}
		deviceService.getDevice(device, $scope);
		console.log($scope.device)
	}
	
	
	$scope.updateDevice = function() {
		if($scope.device.pendingBill) {
			alert("Device can't be updated. There is still a pending bill.");
		}else {
			var device = new $scope.Device();
			for(bill of $scope.device.bills) {
				var date = new Date(device.installationDate.month + "/" + device.installationDate.day + "/" + device.installationDate.year);
				var date2 = new Date(bill.readingDate.month + "/" + bill.readingDate.day + "/" + bill.readingDate.year);
				if(date > date2) {
					alert("Device can't be updated. Installation date must not overlap reading date");
					break;
				}else {
					deviceService.updateDeviceDate(device);
				}

			}
			
		}
	}
	
	$scope.addBill = function() {
		var bill = new $scope.Bill();
		deviceService.addBillToDevice(bill, $scope);
	}
	
	$scope.toDelete = function(bill) {
		if(bill.remove) {
			$scope.toBeDeletedBills.push(bill.id);
		}else {
			var index = $scope.toBeDeletedBills.indexOf(bill.id);
			$scope.toBeDeletedBills.splice(index, 1);
		}
	}
	
	$scope.deleteBills = function() {
		var bills = {
				billsIds: $scope.toBeDeletedBills
		}
		if(bills.billIds != undefined) {
			for(bill of $scope.toBeDeletedBills) {
				var index = $scope.device.billsIds.indexOf(bill);
				$scope.device.billsIds.splice(index, 1);
				$scope.device.bills.splice(index, 1);
			}
			deviceService.deleteBills(bills, $scope);
		}else {
			alert('no bills to be deleted.')
		}
	}
	
	$scope.editBill = function(bill) {
		if(!bill.status) {
			$scope.showModal(bill);
			console.log(bill);
		}else {
			alert("Can't edit already paid bill.");
		}
	}
	
	$scope.updateBill = function() {
		var bill = new $scope.Bill();
		console.log(bill);
		deviceService.updateBill(bill)
	}
	
	$scope.Device = function() {
		this.id = $scope.device.id;
		this.installationDate = {
				month: $scope.device.installation.month.id,
				day: $scope.device.installation.day,
				year: $scope.device.installation.year
		}
	}
	
	$scope.Bill = function() {
		this.id = $scope.editBill.id;
		this.reading = $scope.editBill.reading;
		this.readingDate = {
				month:  $scope.editBill.readingDate.month.id,
				day:  $scope.editBill.readingDate.day,
				year:  $scope.editBill.readingDate.year
		}
	}
	
	$scope.changeLocation = function() {
		$location.path('/EditCustomerDetails/' + $scope.device.ownerId);
	}
	
	$scope.showModal = function(bill) {
		var modalWrap = angular.element(document.querySelector('#editModalWrapper'));
		var modalContent =  angular.element(document.querySelector('#editModal'));
		var close = angular.element(document.querySelector('#editClose'));
		var month = bill.readingDate.month;
		
		modalWrap.addClass('showModalWrap');
		modalContent.addClass('showModalContent');
		
		for(m of $scope.$parent.months) {
			if(m.id == month) {
				month = m;
				break;
			}
		}
		
		$scope.editBill.id = bill.id;
		$scope.editBill.reading = bill.reading;
		$scope.editBill.readingDate = bill.readingDate;
		$scope.editBill.readingDate.month = month;
		$scope.editBill.amount = bill.amount;
		
		close.on('click', function() {
				modalWrap.removeClass('showModalWrap');
				modalContent.removeClass('showModalContent');
		})
		
	}
	
	$scope.clear = function() {
		
	}
	
	$scope.getDevice();
})

deviceMod.controller('addDeviceCtrl', function($scope, deviceService) {
	
	$scope.device = {
			meterNumber: ""
	}
	
	$scope.addDevice = function() {
		var device = new $scope.Device();
		deviceService.addDevice(device, $scope);
		$scope.hideModal();
		$scope.clear();
	}
	
	$scope.Device = function() {
		this.meterNumber = $scope.device.meterNumber;
		this.pendingBill = false;
	}
	
	$scope.cancel = function() {
		$scope.clear();
		console.log("Sdsd ")
	}
	
	$scope.clear = function() {
		$scope.device.meterNumber = "";
	}
	
	$scope.hideModal = function(bill) {
		var modalWrap = angular.element(document.querySelector('#modalDeviceWrapper'));
		var modalContent =  angular.element(document.querySelector('#modalDevice'));
		var close = angular.element(document.querySelector('#closeDevice'));
		
		modalWrap.removeClass('showModalWrap');
		modalContent.removeClass('showModalContent');	
	}

})