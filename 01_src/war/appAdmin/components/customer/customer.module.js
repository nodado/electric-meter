var customerMod = angular.module('customerMod', [])

customerMod.controller('listCustomerCtrl', function($scope, $location, customerService) {
	$scope.entries = {
		customers: []
	};
	
	//list all customer.
	$scope.listCustomer = function() {
		customerService.getCustomers($scope.entries);
	}
	
	$scope.editCustomer = function(customer) {
		$location.path('/EditCustomerDetails/'+customer.id);
	}
	
	$scope.sort = function(keyName) {
		$scope.sortKey = keyName;
		$scope.reverse = !$scope.reverse
		console.log($scope.reverse)
	}
	
	
	$scope.listCustomer();
})

customerMod.controller('customerDetailsCtrl', function($scope, customerService, deviceService) {	
	$scope.customer = {
			firstName: "",
			lastName: "",
			middleName: "",
			gender: $scope.$parent.gender[0],
			address: "",
			birthday: {
				month: $scope.$parent.months[0],
				day: $scope.$parent.days[0],
				year: $scope.$parent.years[0]
			},
			email: "",
			username: "",
			password: "",
			devices: []
	}
	
	$scope.disable = false;
	
	$scope.entries = {
			availableDevices: [],
			devices: []
	}
	
	$scope.device = {
			customerDevice: {},
			deviceBill: {}
	}
	
	$scope.installationDate =  {
			month: "",
			day: "",
			year: ""
	}
		
	$scope.getAvailableDevices = function() {
		deviceService.getDevicesNoOwner($scope.entries, $scope.device);
	}
	
	$scope.addDevice = function() {
		$scope.showModal('#installDeviceWrapper', '#installDeviceModal', '#installDeviceClose');
		$scope.getAvailableDevices();
	}
	
	$scope.submitCustomer = function() {
		var customer = new $scope.Customer();
		customerService.addCustomer(customer, $scope);
	}
	
	$scope.deleteDevice = function(device) {
		console.log(device)
		if(device.pendingStatus) {
			alert("Can't delete a device with a pending bill.");
		}else {
			var index = $scope.entries.devices.indexOf(device);
			$scope.entries.devices.splice(index, 1);
		}
	}
	
	$scope.installDevice = function() {
		if($scope.installationDate.month.id !==  "0") {
			if($scope.installationDate.day !== "Day") {
				if($scope.installationDate.year !== "Year") {
					$scope.device.customerDevice.installationDate = {
							month: $scope.installationDate.month.id,
							day: $scope.installationDate.day,
							year: $scope.installationDate.year
					}
					$scope.entries.devices.push($scope.device.customerDevice);
					$scope.customer.devices.push($scope.device.customerDevice.id);
					$scope.hideModal();
				}else {
					alert("Year is required.");
				}
			}else {
				alert("Day is required.");
			}
		}else {
			alert("Month is required.");
		}
	}
	
	//constructor
	$scope.Customer = function() {
		this.firstName = $scope.customer.firstName;
		this.middleName = $scope.middleName;
		this.lastName = $scope.customer.lastName;
		this.gender = $scope.customer.gender;
		this.birthday = {
				month: $scope.customer.birthday.month.id,
				day: $scope.customer.birthday.day,
				year: $scope.customer.birthday.year
		};
		this.email = $scope.customer.email;
		this.username = $scope.customer.username;
		this.password = $scope.customer.password;
		this.devices = $scope.customer.devices;
		this.address = $scope.customer.address;
	}
	
	$scope.clear = function() {
		$scope.customer.firstName = "";
		$scope.customer.lastName = "";
		$scope.customer.gender = $scope.gender[0];
		$scope.customer.birthday.month = $scope.months[0];
		$scope.customer.birthday.day = $scope.days[0];
		$scope.customer.birthday.year = $scope.years[0];
		$scope.customer.email = "";
		$scope.customer.username = "";
		$scope.customer.password = "";
		$scope.customer.confirmPassword = "";
		$scope.customer.address = "";
		
		$scope.entries.devices = [];
	}
	

	$scope.showModal = function() {
		var modalWrap = angular.element(document.querySelector('#installDeviceWrapper'));
		var modalContent =  angular.element(document.querySelector('#installDeviceModal'));
		var close = angular.element(document.querySelector('#installDeviceClose'));
		
		modalWrap.addClass('showModalWrap');
		modalContent.addClass('showModalContent');
		
		close.on('click', function() {
				modalWrap.removeClass('showModalWrap');
				modalContent.removeClass('showModalContent');
		})
		
		$scope.installationDate.month = $scope.months[0];
		$scope.installationDate.day = $scope.days[0];
		$scope.installationDate.year = $scope.years[0];
	}
	
	$scope.hideModal = function() {
		var modalWrap = angular.element(document.querySelector('#installDeviceWrapper'));
		var modalContent =  angular.element(document.querySelector('#installDeviceModal'));
		var close = angular.element(document.querySelector('#installDeviceClose'));
		
		modalWrap.removeClass('showModalWrap');
		modalContent.removeClass('showModalContent');
	}

	
	$scope.initDate= function() {
		for(var i = 1; i <= 31; i++) {
			$scope.days.push(i + "");
		}
		
		for(var i = 2016; i >= 1975; i--) {
			$scope.years.push(i + "");
		}
	}
	
	$scope.compute = function() {
		$bill.amount = $bill.reading * 10.8584;
	}
	
	$scope.initDate();
})

customerMod.controller('editCustomerDetailsCtrl', function($scope, $routeParams, customerService, deviceService) {
	$scope.customer = {
			firstName: "",
			lastName: "",
			middleName: "",
			gender: "",
			address: "",
			birthday: {
				month: "",
				day: "",
				year: ""
			},
			email: "",
			username: "",
			password: "",
			devices: []
	}
	
	$scope.disable = true;
	
	$scope.entries = {
			devices: [],
			devicesWithBills: []
	};
	
	$scope.device = {
			customerDevice: {}
	}
	
	$scope.installationDate =  {
			month: "",
			day: "",
			year: ""
	}
		
	$scope.bill = {
			device: {},
			readingDate: {
				month: $scope.$parent.months[0],
				day: $scope.$parent.days[0],
				year: $scope.$parent.years[0]
			}
	}
	
	$scope.getCustomer = function() {
		var data = {
				id: $routeParams.id
		}
		customerService.getCustomer(data, $scope);
	}
	
	$scope.submitCustomer = function() {
		var customer = new $scope.Customer();
		customerService.updateCustomer(customer, $scope);
	}
	
	$scope.addDevice = function() {
		$scope.showModal('#installDeviceWrapper', '#installDeviceModal', '#installDeviceClose');
		$scope.getAvailableDevices();
	}
	
	$scope.addBill = function(device) {
		if(device.customerId === 0) {
			alert('Device is not yet registered to customer');
		}else{
			if(!device.pendingBill) {
				$scope.bill.device = device;
				$scope.showModal('#billWrapper', '#billModal', '#billClose');
			}else {
				alert("Can't add bill yet.");
			}
		}
	}

	$scope.addBillToDevice = function() {
		if($scope.bill.readingDate.month.id !== 0) { 
			if($scope.bill.readingDate.day != "Day") {
				if($scope.bill.readingDate.year != "Year") {
					var bill = new $scope.Bill();
					deviceService.addBill(bill, $scope);
					$scope.hideModal('#billWrapper', '#billModal', '#billClose');
					$scope.clear();
				}else {
					alert("Year is required.")
				}
			}else {
				alert("Day is required.");
			}
		}else {
			alert("Month is required.")
		}
	}
	
	$scope.deleteDevice = function(device) {
		console.log(device)
		if(device.pendingBill) {
			alert("Can't delete a device with a pending bill.");
		}else {
			var index = $scope.entries.devices.indexOf(device);
			$scope.entries.devices.splice(index, 1);
		}
	}
	
	
	$scope.getAvailableDevices = function() {
		deviceService.getDevicesNoOwner($scope.entries, $scope.device);
	}
	
	$scope.Customer = function() {
		this.id = $scope.customer.id;
		this.devices = [];
		for(device of $scope.entries.devices) {
			this.devices.push(device.id);
		}
	}
	
	$scope.Bill = function() {
		this.customerId = $scope.customer.id;
		this.deviceId = $scope.bill.device.id;
		this.readingDate = {
				month: $scope.bill.readingDate.month.id,
				day: $scope.bill.readingDate.day,
				year: $scope.bill.readingDate.year
		}
		this.reading = $scope.bill.reading;
		this.amount = $scope.bill.amount;
	}
	
	$scope.installDevice = function() {
		if($scope.installationDate.month.id !==  "0") {
			if($scope.installationDate.day !== "Day") {
				if($scope.installationDate.year !== "Year") {
					$scope.device.customerDevice.installationDate = {
							month: $scope.installationDate.month.id,
							day: $scope.installationDate.day,
							year: $scope.installationDate.year
					}
					$scope.entries.devices.push($scope.device.customerDevice);
				}else {
					alert("Year is required.");
				}
			}else {
				alert("Day is required.");
			}
		}else {
			alert("Month is required.");
		}
	}
	
	$scope.showModal = function(wrapperId, modalId, closeId) {
		var modalWrap = angular.element(document.querySelector(wrapperId));
		var modalContent =  angular.element(document.querySelector(modalId));
		var close = angular.element(document.querySelector(closeId));
		
		modalWrap.addClass('showModalWrap');
		modalContent.addClass('showModalContent');
		
		close.on('click', function() {
				modalWrap.removeClass('showModalWrap');
				modalContent.removeClass('showModalContent');
		})
		
		$scope.installationDate.month = $scope.months[0];
		$scope.installationDate.day = $scope.days[0];
		$scope.installationDate.year = $scope.years[0];
	}
	
	$scope.hideModal = function(wrapperId, modalId, closeId) {
		var modalWrap = angular.element(document.querySelector(wrapperId));
		var modalContent =  angular.element(document.querySelector(modalId));
		var close = angular.element(document.querySelector(closeId));
		modalWrap.removeClass('showModalWrap');
		modalContent.removeClass('showModalContent');
	}
	
	$scope.clear = function() {
		$scope.bill.reading = "";
		$scope.bill.amount = "";
		$scope.installationDate.month = $scope.months[0];
		$scope.installationDate.day = $scope.days[0];
		$scope.installationDate.year = $scope.years[0];
	}

	$scope.getCustomer();
	console.log($scope.bill.device + "ke")
})