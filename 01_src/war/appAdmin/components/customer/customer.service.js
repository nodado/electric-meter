customerMod.service('customerService', function($http, $httpParamSerializer) {
	
	this.getCustomers = function(data) {
		var url = '/ListCustomer';
		$http.get(url)
			.then(
					response => {
						data.customers = response.data.customerList; 
						console.log( response.data.customerList);
						},
					error => {
						alert('Something went wrong in retrieving customers')
					}
			)
	}
	
	this.getCustomer = function(data, scope) {
		var url = '/GetCustomer';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		};
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							var customer = response.data.customer;
							var ctr = 0;
							var date = new Date(customer.birthday);
							var month = (date.getMonth() + 1);
							var day = date.getDate();
							var year = date.getFullYear();
							
							scope.customer.id = customer.id;
							scope.customer.firstName = customer.firstName;
							scope.customer.lastName = customer.lastName;
							scope.customer.gender = customer.gender;
							scope.customer.email = customer.email;
							scope.customer.username = customer.username;
							scope.customer.devices = customer.myDevices;
						    scope.customer.address = customer.address;
						    
						    for(m of scope.$parent.months) {
						    	if(month == m.id) {
						    	    scope.customer.birthday.month = m.id;
						    	    break;
						    	}
						    	
						    }
						    
						    for(d of scope.$parent.days) {
						    	if(day == d) {
						    		scope.customer.birthday.day = d;
						    		break;
						    	}
						    }
						    
						    for(y of scope.$parent.years) {
						    	if(year == y) {
						    		scope.customer.birthday.year = y;
						    		break;
						    	}
						    }
						    var data = {
						    		devices: scope.customer.devices
						    }

						    this.getCustomerDevices(data,scope);
						}else {
							
						}
					},
					error => {
						
					}
			)
	}
	
	this.addCustomer = function(data, scope) {
		var url = '/RegisterCustomer';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							var customerId = response.data.customer;
						 	for(var d of  scope.entries.devices) {
						    	d.customerId = customerId;
						    }
						    var devices = {
						    		devices: scope.entries.devices
						    }
						    
							this.registerDevice(devices);
						    scope.clear();
						    console.log(data);
						}else {
							alert('add error')
						}				    
					}
			)
	}
	
	this.updateCustomer = function(data, scope) {
		var customer = data;
		var url = '/UpdateCustomer';
		var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, customer, headers)
			.then(
					response => {
						for(device of scope.entries.devices) {
							device.customerId = customer.id;
							console.log(device)
						}
						var data = {
							devices: scope.entries.devices
						}
						this.registerDevice(data);
						alert('update successful')
					}
			)
	}
	
	this.registerDevice = function(data) {
		var url = '/UpdateDevice';
		var header = {
				headers: {
					'Content-Type' : 'application/json'
				}
		}
		$http.post(url, data, header)
			.then(
					response => {
					},
					error => {
						
					}
			 )
	}
	
	this.getCustomerDevices = function(data, scope) {
		var url = '/GetCustomerDevices';
		var headers = {
				headers:  {
					'Content-Type': 'application/json'
				}
		}
		$http.post(url, data, headers)
			.then(
					response=> {
						scope.entries.devices = response.data.devices;
						for(device of scope.entries.devices) {
							var date = new Date(device.installationDate);
							var installedDate = {
									month: date.getMonth() + 1,
									day: date.getDate(),
									year: date.getFullYear()
							}
							device.installationDate = installedDate;
							
							if(!device.pendingBill) {
								scope.entries.devicesWithBills.push(device);
							}
						}
						scope.bill.device = scope.entries.devicesWithBills[0];
					},
					error => {
						
					}
			)
	}
})