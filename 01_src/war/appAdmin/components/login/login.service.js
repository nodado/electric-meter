loginMod.service('loginService', function($http, $localStorage, $location) {
	this.submit = function(data,scope) {
		var url = '/LoginAdmin';
		var headers = {
				headers: {'Content-Type': 'application/json'}
		};
		
		$http.post(url, data, headers)
			.then(
					response => {
						if(response.data.errorList.length === 0) {
							$localStorage.user = response.data.admin;
							$localStorage.login = true;
							$location.path('/Dashboard');
							scope.$parent.login = true;
							scope.isLogin();
						}else {
							scope.invalid = true;
						}
					}
			)
	}
})