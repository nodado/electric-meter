var loginMod = angular.module('loginMod', [])

loginMod.controller('loginCtrl', function($scope, $location, $localStorage,loginService) {
	$scope.user = {
			username: "",
			password: ""
	}
	$scope.submit = function() {
		loginService.submit($scope.user, $scope);
	}
})