package eletricmeter.dao;

import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import eletricmeter.meta.ElectricMeterDeviceMeta;
import eletricmeter.model.ElectricMeterDevice;

public class ElectricMeterDeviceDao {

    public ElectricMeterDevice getElectricMeterDevicebyId(ElectricMeterDevice device) {
        System.out.println("ElectricMeterDeviceDao.getElectricMeterDeviceById " + "start");
        // container for the query result
        ElectricMeterDevice deviceModel = new ElectricMeterDevice();
        
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        deviceModel = Datastore.query(meta)
                        .filter(meta.id.equal(device.getId()))
                        .asSingle();
        
        System.out.println("ElectricMeterDeviceDao.getElectricMeterDeviceById " + "end");
        return deviceModel;
    }
    
    public List<ElectricMeterDevice> getDevicesByStatus(ElectricMeterDevice model) {
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        return Datastore.query(meta).filter(meta.pendingBill.equal(model.getPendingBill())).asList();
    }
    
    public List<ElectricMeterDevice> getCustomerDevicesByStatus(ElectricMeterDevice model) {
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        return Datastore.query(meta).filter(meta.customerId.equal(model.getCustomerId()), meta.pendingBill.equal(model.getPendingBill())).asList();
    }
    
    
    public List<ElectricMeterDevice> getDevicesByInstallationDate(ElectricMeterDevice from, ElectricMeterDevice to) {
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        return Datastore.query(meta).filter(meta.installationDate.greaterThanOrEqual(from.getInstallationDate())).asList();
    }
    
    public List<ElectricMeterDevice> getDevices() {
        return Datastore.query(ElectricMeterDevice.class).asList();
    }
    
    public ElectricMeterDevice getCustomerDevice(ElectricMeterDevice  device) {
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        return Datastore.query(meta).filter(meta.customerId.equal(device.getId())).asSingle();
    }
    
    public List<ElectricMeterDevice> getDevicesNoOwner() {
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        return Datastore.query(meta).filter(meta.customerId.equal(Long.parseLong("0"))).asList();
    }
    
    public void insertElectricMeterDevice(ElectricMeterDevice device) {
        System.out.println("ElectricMeterDeviceDao.insertElectricMeterDevice " + "start");
        
        System.out.println(device.toString());
        
        Transaction transaction = Datastore.beginTransaction();
        
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("ElectricMeterDevice", device.getMeterNumber());
        Key key = Datastore.allocateId(parentKey, "ElectricMeterDevice");
        
        // setting the 'key' and 'id' of the model
        device.setKey(key);
        device.setId(key.getId());
        
        // inserting the item to the datastore
        Datastore.put(device);
        transaction.commit();
        
        System.out.println("ElectricMeterDeviceDao.insertElectricMeterDevice " + "end");
    }
    public List<ElectricMeterDevice> getDeviceList(){
        System.out.println("ElectricMeterDeviceDao.getDeviceList " + "start");
        
        List<ElectricMeterDevice> customerList = Datastore.query(ElectricMeterDevice.class).asList();

        System.out.println("ElectricMeterDeviceDao.getDeviceList " + "end");
        return customerList;    
    }
    
    public List<ElectricMeterDevice> getDeviceListByUserId(ElectricMeterDevice device){
        System.out.println("ElectricMeterDeviceDao.getDeviceByUserIdList " + "start");
        
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        
        List<ElectricMeterDevice> deviceList = Datastore.query(meta).filter(meta.customerId.equal(device.getCustomerId())).asList();

        System.out.println("ElectricMeterDeviceDao.getDeviceByUserIdList " + "end");
        return deviceList;    
    }
    
    public void updateElectricMeterDevice(ElectricMeterDevice device) {
        System.out.println("ElectricMeterDeviceDao.updateElectricMeterDevice " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // updating the item to the datastore
        Datastore.put(device);
        
        transaction.commit();
        
        System.out.println("ElectricMeterDeviceDao.updateElectricMeterDevice " + "end");
    }
    
    public ElectricMeterDevice getDevicerById(ElectricMeterDevice model) {
        ElectricMeterDeviceMeta meta = ElectricMeterDeviceMeta.get();
        return Datastore.query(meta).filter(meta.id.equal(model.getId())).asSingle();
    }
    
    public void deleteElectricMeterDevice(ElectricMeterDevice device) {
        System.out.println("ElectricMeterDeviceDao.deleteElectricMeterDevice " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // deleting the item in the datastore
        Datastore.delete(device.getKey());
        transaction.commit();
        
        System.out.println("ElectricMeterDeviceDao.deleteElectricMeterDevice " + "end");
    }
    
    
}
