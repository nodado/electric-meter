package eletricmeter.dao;

import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import eletricmeter.meta.ElectricMeterBillMeta;
import eletricmeter.model.ElectricMeterBill;


public class ElectricMeterBillDao {
    
    public ElectricMeterBill getElectricMeterBillbyId(ElectricMeterBill bill) {
        System.out.println("ElectricMeterBillDao.getElectricMeterBillById " + "start" + bill.getId());
        // container for the query result
        ElectricMeterBill billModel = new ElectricMeterBill();
        
        ElectricMeterBillMeta meta = ElectricMeterBillMeta.get();
        billModel = Datastore.query(meta)
                        .filter(meta.id.equal(bill.getId()))
                        .asSingle();
        
        System.out.println("ElectricMeterBillDao.getElectricMeterBillById " + "end" + Datastore.query(meta)
        .filter(meta.id.equal(bill.getId()))
        .asSingle());
        return billModel;
    }
    
    public ElectricMeterBill insertElectricMeterBill(ElectricMeterBill bill) {
        System.out.println("ElectricMeterBillDao.insertElectricMeterBill " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("ElectricMeterBill", (long) bill.getReading() );
        Key key = Datastore.allocateId(parentKey, "ElectricMeterBill");
        
        // setting the 'key' and 'id' of the model
        bill.setKey(key);
        bill.setId(key.getId());
        
        // inserting the item to the datastore
        Datastore.put(bill);
        transaction.commit();
        
        System.out.println("ElectricMeterBillDao.insertElectricMeterBill " + "end");
        return bill;
    }
    
    public void updateElectricMeterBill(ElectricMeterBill bill) {
        System.out.println("ElectricMeterBillDao.updateElectricMeterBill " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // updating the item to the datastore
        Datastore.put(bill);
        
        transaction.commit();
        
        System.out.println("ElectricMeterBillDao.updateElectricMeterBill " + "end");
    }
    
    public void deleteElectricMeterBill(ElectricMeterBill bill) {
        System.out.println("ElectricMeterBillDao.deleteElectricMeterBill " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // deleting the item in the datastore
        Datastore.delete(bill.getKey());
        transaction.commit();
        
        System.out.println("ElectricMeterBillDao.deleteElectricMeterBill " + "end");
    }
    
    public List<ElectricMeterBill> getBillList(){
        System.out.println("ElectricMeterBillDao.getBillList " + "start");
        
        List<ElectricMeterBill> billList = Datastore.query(ElectricMeterBill.class).asList();

        System.out.println("ElectricMeterBillDao.getBillList " + "end");
        return billList;
    }
    
    public List<ElectricMeterBill> getBillListById(ElectricMeterBill bill){
        System.out.println("ElectricMeterBillDao.getBillList " + "start");
        

        ElectricMeterBillMeta meta = ElectricMeterBillMeta.get();
        
        List<ElectricMeterBill> billList = Datastore.query(meta).filter(meta.ownerId.equal(bill.getOwnerId())).asList();


        System.out.println("ElectricMeterBillDao.getBillList " + "end");
        return billList;
    }
    
}
