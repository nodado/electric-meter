package eletricmeter.dao;

import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import eletricmeter.meta.AdminMeta;
import eletricmeter.meta.CustomerMeta;
import eletricmeter.model.Admin;
import eletricmeter.model.Customer;

public class CustomerDao {
    public Customer getCustomerbyName(Customer customer) {
        System.out.println("CustomerDao.getCustomerByName " + "start");
        // container for the query result
        Customer customerModel = new Customer();
        
        CustomerMeta meta = CustomerMeta.get();
        customerModel = Datastore.query(meta)
                        .filter(meta.username.equal(customer.getUsername()))
                        .asSingle();
        
        System.out.println("CustomerDao.getCustomerByName " + "end");
        return customerModel;
    }

    public Customer insertCustomer(Customer customer) {
        System.out.println("CustomerDao.insertCustomer " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("Customer", customer.getUsername());
        Key key = Datastore.allocateId(parentKey, "Customer");
        
        // setting the 'key' and 'id' of the model
        customer.setKey(key);
        customer.setId(key.getId());
        
        // inserting the item to the datastore
        Datastore.put(customer);
        transaction.commit();
        
        
        System.out.println("CustomerDao.insertCustomer " + "end");
        return customer;
    }
    public Customer getAdminbyUserAndPassword(Customer customer){
        System.out.println("CustomerDao.getCustomerbyUserAndPassword " + "start");
        Customer customerModel = new Customer();
        
        
            CustomerMeta meta = CustomerMeta.get();
            
            customerModel = Datastore.query(meta)
                        .filter(meta.username.equal(customer.getUsername()),
                        meta.password.equal(customer.getPassword()))
                        .asSingle();
            

        System.out.println("CustomerDao.getCustomerbyUserAndPassword " + "end");
        return customerModel;
    }
    
    
    public void updateCustomer(Customer customer) {
        System.out.println("CustomerDao.insertCustomer " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // updating the item to the datastore
        Datastore.put(customer);
        
        transaction.commit();
        
        System.out.println("CustomerDao.insertCustomer " + "end");
    }

    public Customer getCustomerbyId(Customer customer) {
        System.out.println("CustomerDao.getCustomerById " + "start");
        // container for the query result
        Customer customerModel = new Customer();
        
        CustomerMeta meta = CustomerMeta.get();
        customerModel = Datastore.query(meta)
                        .filter(meta.id.equal(customer.getId()))
                        .asSingle();
        
        System.out.println("CustomerDao.getCustomerById " + "end");
        return customerModel;
    }

    public void deleteCustomer(Customer customer) {
        System.out.println("CustomerDao.deleteCustomer " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // deleting the item in the datastore
        Datastore.delete(customer.getKey());
        transaction.commit();
        
        System.out.println("CustomerDao.deleteCustomer " + "end");
    }
    
    public List<Customer> getCustomerList() {
        System.out.println("CustomerDao.getAdminList " + "start");
        
        List<Customer> customerList = Datastore.query(Customer.class).asList();

        System.out.println("CustomerDao.getAdminList " + "end");
        return customerList;
    }
}
