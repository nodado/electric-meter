package eletricmeter.dao;

import java.util.List;

import org.slim3.datastore.Datastore;
import org.slim3.datastore.EqualCriterion;
import org.slim3.datastore.FilterCriterion;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

import eletricmeter.meta.AdminMeta;
import eletricmeter.model.Admin;

public class AdminDao {
    public Admin getAdminbyName(Admin admin) {
        System.out.println("AdminDao.getAdminByName " + "start");
        // container for the query result
        Admin adminModel = new Admin();
        
        AdminMeta meta = AdminMeta.get();
        adminModel = Datastore.query(meta)
                        .filter(meta.username.equal(admin.getUsername()))
                        .asSingle();
        
        System.out.println("AdminDao.getAdminByName " + "end");
        return adminModel;
    }

    public Admin getAdminbyId(Admin admin) {
        System.out.println("AdminDao.getAdminById " + "start");
        // container for the query result
        Admin adminModel = new Admin();
        
        AdminMeta meta = AdminMeta.get();
        adminModel = Datastore.query(meta)
                        .filter(meta.id.equal(admin.getId()))
                        .asSingle();
        
        System.out.println("AdminDao.getAdminById " + "end");
        return adminModel;
    }
    public Admin getAdminbyUserAndPassword(Admin admin){
        Admin adminModel = new Admin();
        
        AdminMeta meta = AdminMeta.get();
        adminModel = Datastore.query(meta)
                        .filter(meta.username.equal(admin.getUsername()),
                        meta.password.equal(admin.getPassword()))
                        .asSingle();
        
       
        System.out.println("AdminDao.getAdminByName " + "end");
        return adminModel;
    }
    
    
    public void insertAdmin(Admin admin) {
        System.out.println("AdminDao.insertAdmin " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("Admin", admin.getUsername());
        Key key = Datastore.allocateId(parentKey, "Admin");
        
        // setting the 'key' and 'id' of the model
        admin.setKey(key);
        admin.setId(key.getId());
        
        // inserting the item to the datastore
        Datastore.put(admin);
        transaction.commit();
        
        System.out.println("AdminDao.insertAdmin " + "end");
    }

    public void updateAdmin(Admin admin) {
        System.out.println("AdminDao.updateAdmin " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // updating the item to the datastore
        Datastore.put(admin);
        
        transaction.commit();
        
        System.out.println("AdminDao.updateAdmin " + "end");
    }

    public void deleteAdmin(Admin admin) {
        System.out.println("AdminDao.deleteAdmin " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        // deleting the item in the datastore
        Datastore.delete(admin.getKey());
        transaction.commit();
        
        System.out.println("AdminDao.deleteAdmin " + "end");
    }

    public List<Admin> getAdminList() {
        System.out.println("AdminDao.getAdminList " + "start");
        
        List<Admin> adminList = Datastore.query(Admin.class).asList();

        System.out.println("AdminDao.getAdminList " + "end");
        return adminList;
    }
}
