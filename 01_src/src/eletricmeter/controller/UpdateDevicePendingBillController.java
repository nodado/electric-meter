package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class UpdateDevicePendingBillController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
        JSONObject json = null;
        try{
            json = new JSONObject(new RequestMap(this.request));
            System.out.println(json);
            dto.setId(json.getLong("id"));
            dto.setPendingBill(json.getBoolean("pendingBill"));
            
            service.updateElectricMeterPendingBill(dto);
        }catch(Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
        return null;
    }
}
