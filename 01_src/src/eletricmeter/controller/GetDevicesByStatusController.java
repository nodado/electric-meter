package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class GetDevicesByStatusController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
        JSONObject json = null;
        try {
            json = new JSONObject(new RequestMap(this.request));
            System.out.println(json);
           dto.setPendingBill(json.getBoolean("status"));
           
            json.put("devices", service.getDevicesByStatus(dto).getEntries());
        }catch(Exception e) {
            if(json == null) {
                json = new JSONObject();
            }
            System.out.println("Exception: " + e.getMessage());
        }
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        return null;
    }
}
