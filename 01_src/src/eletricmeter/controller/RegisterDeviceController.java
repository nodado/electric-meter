package eletricmeter.controller;

import java.util.Calendar;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerDto;
import electricmeter.dto.ElectricMeterDeviceDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.CustomerService;
import eletricmeter.service.ElectricMeterDeviceService;

public class RegisterDeviceController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("RegisterDeviceController.run, start");
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
        JSONObject json = null;
            
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);
                     
            validator.add("meterNumber", validator.required());
            
            
            if (validator.validate()) {
                dto.setMeterNumber(json.getString("meterNumber"));
                dto.setPendingBill(json.getBoolean("pendingBill"));
                service.insertDevice(dto);
            } else {
                json = new JSONObject();

                for (int i = 0; i < validator.getErrors().size(); i++) {
                    // add error message
                    dto.addError(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
        } catch (Exception e) {
            // add error message
            dto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", dto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("RegisterDeviceController.run " + "end");

        return null;
    }
}
