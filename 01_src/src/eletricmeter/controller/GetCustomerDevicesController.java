package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class GetCustomerDevicesController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("Get Customer Device Controller start");
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        JSONObject json = null;
        try{
            json = new JSONObject(this.request.getReader().readLine());
            JSONArray jsonDevicesIds = json.getJSONArray("devices");
            int length = jsonDevicesIds.length();
           
            List<ElectricMeterDeviceDto> listDto = new LinkedList<ElectricMeterDeviceDto>();
            for(int ctr = 0; ctr < length; ctr++) {
                ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
                dto.setId(jsonDevicesIds.getLong(ctr));
                listDto.add(dto);
        }
            json.put("devices", service.getCustomerDevices(listDto).getEntries());
            System.out.println(json);
        
        }catch(Exception e) {
            if(json == null) {
                json = new JSONObject();
            }
        }
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        
        System.out.println("Get Customer Device Controller end");
        return null;
    }
}
