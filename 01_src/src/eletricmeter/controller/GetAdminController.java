package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.AdminDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.AdminService;

public class GetAdminController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("getAdminController, start");
        AdminService as = new AdminService();
        AdminDto ad = new AdminDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);

            validator.add("id", validator.required());

            if (validator.validate()) {

                ad.setId(json.getLong("id"));
                json.put("admin", as.getAdmin(ad));
                
            } else {

                for (int i = 0; i < validator.getErrors().size(); i++) {
                    // add error message
                    ad.addError(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
        } catch (Exception e) {
            // add error message
            ad.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", ad.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("getAdminController.run " + "end");

        return null;
    }
}
