package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class RegisterBillToDeviceController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
        JSONObject json = null;
        try{
            ElectricMeterDeviceService service = new ElectricMeterDeviceService();
            json = new JSONObject(this.request.getReader().readLine());
            JSONArray jsonBills = json.getJSONArray("billsId");
            List<Long> billIds = new LinkedList<Long>();
            
            int length = jsonBills.length();
            System.out.println(json);
            for(int ctr = 0; ctr < length; ctr++) {
                billIds.add((jsonBills.getLong(ctr)));
            }
            
            dto.setId(json.getLong("id"));
            dto.setPendingBill(json.getBoolean("pendingBill"));
            dto.setBillId(billIds);
            
            service.registerBilsToDevice(dto);
            
        }catch(Exception e)  {
            if(json == null) {
                json = new JSONObject();
            }
        }
        json.put("errorList", dto.getErrorList());
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        
        return null;
    }
}
