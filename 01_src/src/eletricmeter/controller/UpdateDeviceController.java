package eletricmeter.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.ElectricMeterDeviceService;

public class UpdateDeviceController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("updateDeviceController, start");
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        List<ElectricMeterDeviceDto> deviceList = new LinkedList<ElectricMeterDeviceDto>();
        JSONObject json = null;
        JSONObject jsonDate = null;
        JSONArray jsonArray = null;
        
        DateFormat dateInstalled = new SimpleDateFormat("yyyy-mm-dd");
        String month, year, day;
        
        try{
            json = new JSONObject(this.request.getReader().readLine());
            System.out.println(json);
            jsonArray = json.getJSONArray("devices");
            JSONObject jo;
            int length = jsonArray.length();
            
            for(int i = 0; i < length; i++){
                ElectricMeterDeviceDto device = new ElectricMeterDeviceDto();
                jo = (JSONObject)jsonArray.get(i);
                device.setId(jo.getLong("id"));
                device.setCustomerId(jo.getLong("customerId"));
                jsonDate = (JSONObject) jo.get("installationDate");
                month = String.valueOf(jsonDate.getInt("month"));
                day = String.valueOf(jsonDate.getInt("day"));
                year = String.valueOf(jsonDate.getInt("year"));
 
                device.setInstallationDate(dateInstalled.parse(year + "-" + month + "-" +  day));
                deviceList.add(device);
            }
           service.registerDevicesToOwner(deviceList);
        }catch(Exception e) {
            // add error message
            System.out.println(e.toString());
        }
        
        System.out.println("updateDeviceController, end");
        return null;
    }
}
