package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterBillListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.ElectricMeterBillService;

public class ListMeterBillsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("ListMeterBillController.run " + "start");
        ElectricMeterBillService billService = new ElectricMeterBillService();
        ElectricMeterBillListDto billDto = new ElectricMeterBillListDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject();

            billDto = billService.getBillList();

            json.put("billList", billDto.getEntries());
        } catch (Exception e) {
            // add error message
            billDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", billDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListMeterBillController.run " + "end");
        return null;
    }
}
