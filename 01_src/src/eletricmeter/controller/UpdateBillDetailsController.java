package eletricmeter.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterBillDto;
import eletricmeter.service.ElectricMeterBillService;

public class UpdateBillDetailsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterBillService service = new ElectricMeterBillService();
        ElectricMeterBillDto dto = new ElectricMeterBillDto();
        try {
            JSONObject json = new JSONObject(this.request.getReader().readLine());
            JSONObject readingDateJson = (JSONObject)json.get("readingDate");
            Calendar c = Calendar.getInstance();
            c.set(readingDateJson.getInt("year"),readingDateJson.getInt("month"),readingDateJson.getInt("day"));
            dto.setId(json.getLong("id"));
            dto.setReading(json.getDouble("reading"));
            dto.setReadingDate(c.getTime());
            service.updateBillDetails(dto);
            System.out.println(json);
        }catch(Exception e) {
            System.out.println("Exception:" + e.getMessage());
        }
        return null;
    }
}
