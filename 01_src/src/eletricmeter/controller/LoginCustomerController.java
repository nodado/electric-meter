package eletricmeter.controller;

import java.util.HashMap;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.AdminDto;
import electricmeter.dto.CustomerDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.AdminService;
import eletricmeter.service.CustomerService;

public class LoginCustomerController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("LoginCustomerController start, end");
        CustomerService cs= new CustomerService();
        CustomerDto cd = new CustomerDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            System.out.println(""+json.toString());
            
            cd.setUsername(json.getString("username"));
            cd.setPassword(json.getString("password"));
            
            System.out.println("Customer ni nako: " + cs.searchCustomer(cd).toString());
            
            json.put("customer", cs.searchCustomer(cd));
        } catch (Exception e) {
            // add error message
            cd.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }
        json.put("errorList", cd.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());
        
        return null;
    }
}
