package eletricmeter.controller;

import java.util.ArrayList;
import java.util.Calendar;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.CustomerService;

public class UpdateCustomerController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("UpdateCustomerController, start");
        CustomerService cs = new CustomerService();
        CustomerDto cd = new CustomerDto();
        JSONObject json = null;

        try {
            json = new JSONObject(this.request.getReader().readLine());
            System.out.println(json);    
            JSONArray r = json.getJSONArray("devices");
            ArrayList<Long> deviceIdList = new ArrayList<Long>();
            for(int i = 0; i < r.length(); i++){
                deviceIdList.add(r.getLong(i));
            }
            cd.setId(json.getLong("id"));
            System.out.println(json.getLong("id"));
            cd.setMyDevices(deviceIdList);
            cs.updateCustomer(cd);
         
        } catch (Exception e) {
            // add error message
            cd.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        System.out.println("UpdateCustomerController.run " + "end");

        return null;
    }
}
