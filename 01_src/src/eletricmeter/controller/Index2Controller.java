package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class Index2Controller extends Controller {

    @Override
    public Navigation run() throws Exception {        
        return forward("/appAdmin/Index.html");
    }
}
