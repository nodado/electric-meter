package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class DeleteBillsOfDeviceController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("DeleteBillsOfDevice Controller start");
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        try{
            ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
            JSONObject json = new JSONObject(this.request.getReader().readLine());
            JSONArray jsonBill = json.getJSONArray("billsId");
            List<Long> billIds = new LinkedList<Long>();
            int length = jsonBill.length();
            
            for(int ctr = 0; ctr < length; ctr++) {
                billIds.add(jsonBill.getLong(ctr));
            }
            
            dto.setId(json.getLong("id"));
            dto.setBillId(billIds);
         //   service.deleteBillsOfDevice(dto);
        }catch(Exception e) {
            System.out.println("Exception:" + e.getMessage());
        }
        System.out.println("DeleteBillsOfDevice Controller end");
        return null;
    }
}
