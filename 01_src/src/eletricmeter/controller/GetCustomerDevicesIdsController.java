package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.CustomerDto;
import eletricmeter.service.CustomerService;

public class GetCustomerDevicesIdsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        CustomerService service = new CustomerService();
        JSONObject json = new JSONObject(new RequestMap(this.request));
        CustomerDto dto = new CustomerDto();
        dto.setId(json.getLong("id"));
        
        json.put("devicesIds", service.getCustomerDevicesIds(dto));
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        return null;
    }
}
