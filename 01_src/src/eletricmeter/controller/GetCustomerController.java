package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.CustomerService;

public class GetCustomerController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("getCustomerController, start");
        CustomerService cs = new CustomerService();
        CustomerDto cd = new CustomerDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);

            validator.add("id", validator.required());

            if (validator.validate()) {
                cd.setId(Long.parseLong(json.getString("id")));
                json.put("customer", cs.getCustomer(cd));
                
            } else {

                for (int i = 0; i < validator.getErrors().size(); i++) {
                    // cdd error message
                    cd.addError(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
        } catch (Exception e) {
            // cdd error message
            cd.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // cdd error messages to the json object.
        json.put("errorList", cd.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("getCustomerController.run " + "end");

        return null;
    }
}
