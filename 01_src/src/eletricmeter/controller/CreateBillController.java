package eletricmeter.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.ElectricMeterBillDto;
import eletricmeter.service.ElectricMeterBillService;

public class CreateBillController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("createBillController, start");
        ElectricMeterBillDto dto = new ElectricMeterBillDto();
        ElectricMeterBillService service = new ElectricMeterBillService();
        JSONObject json = null;
        
        try{
          
            
                json = new JSONObject(this.request.getReader().readLine());
                System.out.println("JSON object: " + json.toString());
                JSONObject jsonDate = (JSONObject)json.get("readingDate");
                DateFormat readingDate = new SimpleDateFormat("yyyy-mm-dd");
                String month, year, day;
                
                month = String.valueOf(jsonDate.getInt("month"));
                day = String.valueOf(jsonDate.getInt("day"));
                year = String.valueOf(jsonDate.getInt("year"));
                
                dto.setAmount(json.getDouble("amount"));
                dto.setReading(json.getInt("reading"));
                dto.setReadingDate(readingDate.parse(year + "-" + month + "-" +  day));
                dto.setDeviceId(json.getLong("deviceId"));
                dto.setOwnerId(json.getLong("customerId"));
                
               json.put("bill",  service.insertBill(dto));         
        }
        catch(Exception e){
            // add error message
            System.out.println(e.toString());
        }
        
        json.put("errorList", dto.getErrorList());
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        System.out.println("createBillController, end");
        return null;
    }
}
