package eletricmeter.controller;

import java.util.ArrayList;
import java.util.Calendar;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.AdminDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.AdminService;

public class UpdateAdminController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("registerAdminController, start");
        AdminService as = new AdminService();
        AdminDto ad = new AdminDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);

            validator.add("firstName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("email", validator.required());
            validator.add("gender", validator.required());
            validator.add("birthday", validator.required());

            if (validator.validate()) {
                ad.setId(json.getLong("id"));
                ad.setFirstName(json.getString("firstName"));
                ad.setLastName(json.getString("lastName"));
                ad.setEmail(json.getString("email"));
                ad.setGender(json.getString("gender"));
                
                ArrayList<Boolean> rights = new ArrayList(4);
                JSONObject r = json.getJSONObject("rights");
                rights.add(r.getBoolean("canAdd"));
                rights.add(r.getBoolean("canEdit"));
                rights.add(r.getBoolean("canDelete"));
                rights.add(r.getBoolean("canRights"));
                
                ad.setRights(rights);
                
                JSONObject j = json.getJSONObject("birthday");
                
                Calendar c = Calendar.getInstance();
                c.set(Integer.parseInt(j.getString("year")), Integer.parseInt(j.getString("month")), Integer.parseInt(j.getString("day")));
                ad.setBirthday(c.getTime());
                
                ad.setRights(rights);

                ad = as.updateAdmin(ad);
            } else {
                json = new JSONObject();

                for (int i = 0; i < validator.getErrors().size(); i++) {
                    // add error message
                    ad.addError(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
        } catch (Exception e) {
            // add error message
            ad.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", ad.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("UpdatePersonController.run " + "end");

        return null;
    }
}
