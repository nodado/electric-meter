package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerDto;
import eletricmeter.service.ElectricMeterBillService;

public class GetBillsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        JSONObject json = null;
        try{
        ElectricMeterBillService service = new ElectricMeterBillService();
        CustomerDto dto = new CustomerDto();
        json = new JSONObject(this.request.getReader().readLine());
        JSONArray jsonBillIds = json.getJSONArray("billsIds");
        List<Long> billIdList = new LinkedList<Long>();
        int length = jsonBillIds.length();
        
        for(int ctr = 0; ctr < length; ctr++) {
            billIdList.add(jsonBillIds.getLong(ctr));
        }
        
        System.out.println(billIdList);
        
        json  = new JSONObject();
        json.put("bills", service.getBills(billIdList));
        }catch(Exception e) {
            if(json == null) {
                json = new JSONObject();
            }
        }
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        return null;
    }
}
