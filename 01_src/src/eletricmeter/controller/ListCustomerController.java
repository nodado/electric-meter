package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.CustomerService;

public class ListCustomerController extends Controller {

    @Override
    public Navigation run() throws Exception {
        CustomerService customerService = new CustomerService();
        CustomerListDto customerListDto = new CustomerListDto();
        JSONObject json = null;

        try {
            json = new JSONObject();

            customerListDto = customerService.getCustomerList();

            json.put("customerList", customerListDto.getEntries());
        } catch (Exception e) {
            // add error message
            customerListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", customerListDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListCustomerController.run " + "end");

        return null;
    }
}
