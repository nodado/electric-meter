package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceListDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.ElectricMeterDeviceService;

public class ListCustomerDevicesController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("ListCustomerDeviceController.run " + "start");
        ElectricMeterDeviceService deviceService = new ElectricMeterDeviceService();
        ElectricMeterDeviceListDto deviceDto = new ElectricMeterDeviceListDto();
        JSONObject json = null;
        
        try {
json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);
                        
            validator.add("customerId", validator.required());
            
            if(validator.validate()){
                deviceDto = deviceService.getDeviceListByCustomerId(json.getLong("customerId"));
            }
            else {
                json = new JSONObject();

                for (int i = 0; i < validator.getErrors().size(); i++) {
                    // add error message
                    deviceDto.addError(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }

            json.put("deviceList", deviceDto.getEntries());
        } catch (Exception e) {
            // add error message
            deviceDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", deviceDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListCustomerDevicesController.run " + "end");
        return null;
    }
}
