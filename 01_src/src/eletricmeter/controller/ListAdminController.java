package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.AdminListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.AdminService;

public class ListAdminController extends Controller {

    @Override
    public Navigation run() throws Exception {
        AdminService adminService = new AdminService();
        AdminListDto adminListDto = new AdminListDto();
        JSONObject json = null;

        try {
            json = new JSONObject();

            adminListDto = adminService.getAdminList();

            json.put("adminList", adminListDto.getEntries());
        } catch (Exception e) {
            // add error message
            adminListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", adminListDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListAdminController.run " + "end");

        return null;
    }
}
