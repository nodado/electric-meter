package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerListDto;
import electricmeter.dto.ElectricMeterDeviceDto;
import electricmeter.dto.ElectricMeterDeviceListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.CustomerService;
import eletricmeter.service.ElectricMeterDeviceService;

public class ListDevicesController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService deviceService = new ElectricMeterDeviceService();
        ElectricMeterDeviceListDto deviceListDto = new ElectricMeterDeviceListDto();
        JSONObject json = null;

        try {
            json = new JSONObject();

            deviceListDto = deviceService.getDeviceList();

            json.put("deviceList", deviceListDto.getEntries());
        } catch (Exception e) {
            // add error message
            deviceListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", deviceListDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListDeviceController.run " + "start, end");
        return null;
    }
}
