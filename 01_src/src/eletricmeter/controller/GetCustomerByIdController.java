package eletricmeter.controller;

import java.util.HashMap;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.CustomerDto;
import eletricmeter.service.CustomerService;

public class GetCustomerByIdController extends Controller {

    @Override
    public Navigation run() throws Exception {
        CustomerService service = new CustomerService();
        CustomerDto dto = new CustomerDto();
        JSONObject json = new JSONObject(new RequestMap(this.request));
        dto.setId(json.getLong("id"));
        HashMap<String, Object> map = service.getCustomer(dto);
        String firstName =  (String)map.get("firstName");
        String lastName = (String)map.get("lastName");
        System.out.println(firstName + lastName);
        
        HashMap<String, String> toView = new HashMap<String, String>();
        toView.put("firstName", firstName);
        toView.put("lastName", lastName);
        
        json = new JSONObject();
        
        json.put("customer", toView);
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        return null;
    }
}
