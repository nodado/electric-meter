package eletricmeter.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class ListDevicesByInstallationDateController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println(" ListDevicesByInstallationDate start");
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        ElectricMeterDeviceDto fromDto = new ElectricMeterDeviceDto();
        ElectricMeterDeviceDto toDto = new ElectricMeterDeviceDto();
        
        DateFormat birthday = new SimpleDateFormat("yyyy-mm-dd");
        JSONObject json = null;
        try{
            json = new JSONObject(this.request.getReader().readLine());
            JSONObject jsonFrom = (JSONObject)json.get("from");
            JSONObject jsonTo = (JSONObject)json.get("to");
            
            Calendar c = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c.set(jsonFrom.getInt("year"), jsonFrom.getInt("month"), jsonFrom.getInt("day"));
            c2.set(jsonTo.getInt("year"), jsonTo.getInt("month"), jsonTo.getInt("day"));
            
            fromDto.setInstallationDate(c.getTime());
            toDto.setInstallationDate(c2.getTime());
            
      
            System.out.println(birthday.parse(String.valueOf(jsonFrom.getInt("year") + "-" + String.valueOf(jsonFrom.getInt("month") + "-" +  String.valueOf(jsonFrom.getInt("day"))))));
            service.getDevicesByInstallationDate(fromDto, toDto);
            
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("ListDevicesByInstallationDate end");
        return null;
    }
}
