package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;


public class GetDeviceController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        JSONObject json = new JSONObject(new RequestMap(this.request));
        ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
        
        System.out.print(Long.parseLong(json.getString("id")));
        
        dto.setId(Long.parseLong(json.getString("id")));
        
        json.put("device", service.getDevice(dto));
        json.put("errorList", dto.getErrorList());
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        
        return null;
    }
}
