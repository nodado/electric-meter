package eletricmeter.controller;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.CustomerDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.CustomerService;

public class RegisterCustomerController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("registerCustomerController, start");
        CustomerService service = new CustomerService();
        CustomerDto dto = new CustomerDto();
        JSONObject json = null;
        HashMap<String, Object> map = new HashMap<String, Object>();
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);
            
            
            validator.add("firstName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("email", validator.required());
            validator.add("gender", validator.required());
            validator.add("birthday", validator.required());
            validator.add("address", validator.required());
            validator.add("username", validator.required());
            validator.add("password", validator.required());
            
            if (validator.validate()) {
                dto.setFirstName(json.getString("firstName"));
                dto.setLastName(json.getString("lastName"));
                dto.setEmail(json.getString("email"));
                dto.setGender(json.getString("gender"));
                
                JSONObject j = json.getJSONObject("birthday");
                
                Calendar c = Calendar.getInstance();
                c.set(j.getInt("year"), j.getInt("month"),j.getInt("day"));
                dto.setBirthday(c.getTime());
                dto.setAddress(json.getString("address"));
               
                dto.setUsername(json.getString("username"));
                dto.setPassword(json.getString("password"));
                
                JSONArray r = json.getJSONArray("devices");
                int length = r.length();
                ArrayList<Long> deviceIdList = new ArrayList<Long>();
                
                for(int i = 0; i < length; i++){
                    deviceIdList.add(r.getLong(i));
                }
                    
                dto.setMyDevices(deviceIdList);
                dto = service.insertCustomer(dto);
                json.put("customer",dto.getId());
                
            } else {
                json = new JSONObject();

                for (int i = 0; i < validator.getErrors().size(); i++) {
                    // add error message
                    dto.addError(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
        } catch (Exception e) {
            // add error message
            dto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", dto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());
        return null;
    }
}
