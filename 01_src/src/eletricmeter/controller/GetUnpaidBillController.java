package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import eletricmeter.service.ElectricMeterBillService;

public class GetUnpaidBillController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("GetUnpaidControllerBill start");
        ElectricMeterBillService service = new ElectricMeterBillService();
        JSONObject json = null;
        try {
            json = new JSONObject(this.request.getReader().readLine());
            System.out.println(json);
            JSONArray jsonBillIds = json.getJSONArray("billsId");
            List<Long> billIdList = new LinkedList<Long>();
            int length = jsonBillIds.length();
            
            for(int ctr = 0; ctr < length; ctr++) {
                billIdList.add(jsonBillIds.getLong(ctr));
            }
            
            json.put("unpaidBill", service.getUnpaidBill(billIdList));
        }catch(Exception e) {
            if(json == null) {
                json = new JSONObject();
            }
        }
        
        System.out.println(json);
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        System.out.println("GetUnpaidControllerBill end");
        return null;
    }
}
