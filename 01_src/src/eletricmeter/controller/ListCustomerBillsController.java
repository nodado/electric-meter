package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterBillListDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.service.ElectricMeterBillService;

public class ListCustomerBillsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("ListCustomerBillsController.run " + "start");
        ElectricMeterBillService billService = new ElectricMeterBillService();
        ElectricMeterBillListDto billDto = new ElectricMeterBillListDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            JSONValidators validator = new JSONValidators(json);
                        
            validator.add("customerId", validator.required());
            
            if(validator.validate()){
                billDto = billService.getBillListByUserId(json.getLong("customerId"));
            }
            else {
            json = new JSONObject();

            for (int i = 0; i < validator.getErrors().size(); i++) {
                // add error message
                billDto.addError(validator.getErrors().get(i));
                System.out.println(validator.getErrors().get(i));
            }
        }
            json.put("billList", billDto.getEntries());
        } catch (Exception e) {
            // add error message
            billDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", billDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListCustomerBillsController.run " + "end");
        return null;
    }
}
