package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import electricmeter.dto.ElectricMeterDeviceListDto;
import eletricmeter.service.ElectricMeterDeviceService;


public class ListDevicesNoOwnerController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        JSONObject json = new JSONObject();
        ElectricMeterDeviceListDto listDto = service.getDevicesNoOwner();
        
        json.put("devices", listDto.getEntries());
        json.put("errorList",listDto.getErrorList());
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        return null;
    }
}
