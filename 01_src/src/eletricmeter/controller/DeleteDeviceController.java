package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.model.ElectricMeterDevice;
import eletricmeter.service.ElectricMeterDeviceService;
import electricmeter.utils.GlobalConstants;

public class DeleteDeviceController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService deviceService = new ElectricMeterDeviceService();
        ElectricMeterDeviceDto deviceDto = new ElectricMeterDeviceDto();
        JSONObject json = null;

        try {
            json = new JSONObject(this.request.getReader().readLine());

            deviceDto.setId(json.getLong("id"));

            deviceService.deleteDevice(deviceDto);
        } catch (Exception e) {
            // add error message
            deviceDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        System.out.println("DeletePersonController.run " + "end");

        return null;
    }
}
