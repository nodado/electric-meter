package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.AdminDto;
import electricmeter.dto.CustomerDto;
import electricmeter.utils.GlobalConstants;
import electricmeter.utils.JSONValidators;
import eletricmeter.model.Admin;
import eletricmeter.service.AdminService;
import eletricmeter.service.CustomerService;

public class LoginAdminController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("LoginAdminController start, end");
        System.out.println("registerCustomerController, start");
        AdminService as= new AdminService();
        AdminDto ad = new AdminDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(this.request.getReader().readLine());
            
            System.out.println(""+json.toString());
            
            ad.setUsername(json.getString("username"));
            ad.setPassword(json.getString("password"));
        
            json.put("admin", as.searchAdmin(ad));
        } catch (Exception e) {
            // add error message
            ad.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }
        json.put("errorList", ad.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());
        return null;
    }
}
