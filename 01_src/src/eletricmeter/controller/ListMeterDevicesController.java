package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.service.ElectricMeterDeviceService;

public class ListMeterDevicesController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("ListMeterDeviceController.run " + "start");
        ElectricMeterDeviceService deviceService = new ElectricMeterDeviceService();
        ElectricMeterDeviceListDto deivceDto = new ElectricMeterDeviceListDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject();

            deivceDto = deviceService.getDeviceList();

            json.put("deviceList", deivceDto.getEntries());
        } catch (Exception e) {
            // add error message
            deivceDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", deivceDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("ListDeviceController.run " + "end");
        return null;

    }
}
