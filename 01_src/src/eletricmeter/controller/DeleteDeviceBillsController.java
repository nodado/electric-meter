package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterBillDto;
import eletricmeter.service.ElectricMeterBillService;

public class DeleteDeviceBillsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("DeleteDeviceBillsController start");
        ElectricMeterBillService service = new ElectricMeterBillService();
        JSONObject json = null;
        try{
            json = new JSONObject(this.request.getReader().readLine());
            JSONArray jsonBills = json.getJSONArray("billsIds");
            List<ElectricMeterBillDto> listDto = new LinkedList<ElectricMeterBillDto>();
            int length = jsonBills.length();
            for(int ctr = 0; ctr < length; ctr++) {
                ElectricMeterBillDto billDto = new ElectricMeterBillDto();
                billDto.setId(jsonBills.getLong(ctr));
                listDto.add(billDto);
            }
            service.deleteBills(listDto);
        }catch(Exception e) {
            
        }
        System.out.println("DeleteDeviceBillsController.run " + "end");

        return null;
    }
}
