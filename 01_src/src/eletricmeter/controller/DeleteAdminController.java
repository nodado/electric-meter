package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.utils.GlobalConstants;
import electricmeter.dto.AdminDto;
import eletricmeter.service.AdminService;

public class DeleteAdminController extends Controller {

    @Override
    public Navigation run() throws Exception {
        AdminService adminService = new AdminService();
        AdminDto adminDto = new AdminDto();
        JSONObject json = null;

        try {
            json = new JSONObject(this.request.getReader().readLine());

            adminDto.setId(json.getLong("id"));

            adminDto = adminService.deleteAdmin(adminDto);
        } catch (Exception e) {
            // add error message
            adminDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX
                + e.getMessage());
            System.out.println(e.toString());
        }

        // add error messages to the json object.
        json.put("errorList", adminDto.getErrorList());

        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(json.toString());

        System.out.println("DeleteAdminController.run " + "end");

        return null;
    }
}
