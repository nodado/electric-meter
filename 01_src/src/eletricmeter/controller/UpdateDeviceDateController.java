package eletricmeter.controller;

import java.util.Calendar;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class UpdateDeviceDateController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
        try{
            JSONObject json = new JSONObject(this.request.getReader().readLine());
            JSONObject jsonBirthday = (JSONObject) json.get("installationDate");
            Calendar c = Calendar.getInstance();
            c.set(jsonBirthday.getInt("year"),jsonBirthday.getInt("month"),jsonBirthday.getInt("day"));
            dto.setId(json.getLong("id"));
            dto.setInstallationDate(c.getTime());
            System.out.println(json);
            service.updateDeviceDate(dto);
        }catch(Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
        return null;
    }
}
