package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterBillDto;
import eletricmeter.service.ElectricMeterBillService;

public class GetDeviceBillsController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("GetDeviceBills start");
        ElectricMeterBillService service = new ElectricMeterBillService();
        JSONObject json = null;
        try {
            json = new JSONObject(this.request.getReader().readLine());
            JSONArray jsonBills = json.getJSONArray("billsIds");
            List<ElectricMeterBillDto> listDto = new LinkedList<ElectricMeterBillDto>();
            int length  = jsonBills.length();
            
            for(int ctr = 0; ctr < length; ctr++) {
                ElectricMeterBillDto dto = new ElectricMeterBillDto();
                dto.setId(jsonBills.getLong(ctr));
                listDto.add(dto);
            }
            System.out.println(json);
            json.put("bills", service.getBillsOfDevice(listDto).getEntries());
        }catch(Exception e) {
            if(json == null) {
                json = new JSONObject();
            }
            System.out.println("Exception:" + e.getMessage());
        }
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        
        System.out.println("GetDeviceBills Controller end");
        return null;
    }
}
