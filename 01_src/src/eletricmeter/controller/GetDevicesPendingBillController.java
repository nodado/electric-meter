package eletricmeter.controller;

import java.util.LinkedList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import electricmeter.dto.ElectricMeterDeviceDto;
import eletricmeter.service.ElectricMeterDeviceService;

public class GetDevicesPendingBillController extends Controller {

    @Override
    public Navigation run() throws Exception {
        System.out.println("GetDevicesPendingBill start");
        ElectricMeterDeviceService service = new ElectricMeterDeviceService();
        JSONObject json = null;
        try{
            json = new JSONObject(this.request.getReader().readLine());
            JSONArray jsonArray = json.getJSONArray("devices");
            List< ElectricMeterDeviceDto> listDto = new LinkedList< ElectricMeterDeviceDto>();
            int length = jsonArray.length();
            
            for(int ctr = 0; ctr < length; ctr++) {
                ElectricMeterDeviceDto dto = new  ElectricMeterDeviceDto();
                dto.setId(jsonArray.getLong(ctr));
                listDto.add(dto);
            }
            json = new JSONObject();
            json.put("devices",service.getDevicesPendingBill(listDto).getEntries());
        }catch(Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        
        System.out.println("GetDevicesPendingBill end");
        return null;
    }
}
