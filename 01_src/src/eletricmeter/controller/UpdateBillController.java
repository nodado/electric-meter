package eletricmeter.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import electricmeter.dto.ElectricMeterBillDto;
import eletricmeter.service.ElectricMeterBillService;

public class UpdateBillController extends Controller {

    @Override
    public Navigation run() throws Exception {
        ElectricMeterBillService service = new ElectricMeterBillService();
        ElectricMeterBillDto dto = new ElectricMeterBillDto();
        JSONObject json = null;
        
        try {
            json = new JSONObject(new RequestMap(this.request));
            System.out.println(json);
            dto.setId(json.getLong("id"));
            dto.setStatus(json.getBoolean("status"));
            service.updateBill(dto);
        }catch(Exception e) {
          
        }
        
        return null;
    }
}
