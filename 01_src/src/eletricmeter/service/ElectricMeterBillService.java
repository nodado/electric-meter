package eletricmeter.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import electricmeter.dto.ElectricMeterBillDto;
import electricmeter.dto.ElectricMeterBillListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.dao.ElectricMeterBillDao;
import eletricmeter.model.ElectricMeterBill;


public class ElectricMeterBillService {
  ElectricMeterBillDao bd = new ElectricMeterBillDao();
  
  public ElectricMeterBillListDto getBillList() {
      List<ElectricMeterBill> billList = bd.getBillList();
      return this.getBillList(billList);
  }
  
  public ElectricMeterBillListDto getBillListByUserId(long id) {
      ElectricMeterBill model = new ElectricMeterBill();
      model.setOwnerId(id);
      List<ElectricMeterBill> billList = bd.getBillListById(model);
      return this.getBillList(billList);
  }
  
  public List<ElectricMeterBillDto> getBills(List<Long> billIds) {
      List<ElectricMeterBillDto> billList = new LinkedList<ElectricMeterBillDto>();
      for(Long b : billIds) {
          ElectricMeterBillDto dto = new ElectricMeterBillDto();
          ElectricMeterBill model = new ElectricMeterBill();
          model.setId(b);
          ElectricMeterBill resultModel = bd.getElectricMeterBillbyId(model);
          dto.setId(resultModel.getId());
          dto.setDeviceId(resultModel.getId());
          dto.setReading(resultModel.getReading());
          dto.setAmount(resultModel.getAmount());
          dto.setReadingDate(resultModel.getReadingDate());
          dto.setStatus(resultModel.isStatus());
          System.out.println(resultModel.isStatus());
          billList.add(dto);
      }
      return billList;
  }
  
  public ElectricMeterBillListDto getBillList(List<ElectricMeterBill> billList) {
      ElectricMeterBillListDto bdtoList = new ElectricMeterBillListDto();
      
      try {
          if (bdtoList != null) {
              for (ElectricMeterBill bill : billList) {
                  ElectricMeterBillDto blto = new ElectricMeterBillDto();

                  blto.setDeviceId(bill.getDeviceId());
                  blto.setOwnerId(bill.getOwnerId());
                  blto.setReading(bill.getReading());
                  blto.setReadingDate(bill.getReadingDate());
                  blto.setStatus(bill.isStatus());
                  
                  System.out.println(blto.isStatus());
                  bdtoList.getEntries().add(blto);
              }
          }
      } catch (Exception e) {
          bdtoList.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
      }
      
      return bdtoList;
  }
  
  public ElectricMeterBillListDto getBillsOfDevice(List<ElectricMeterBillDto> dtoList) {
      System.out.println("GetBillsOfDevice start");
      ElectricMeterBillListDto listDto = new ElectricMeterBillListDto();
      try {
          for(ElectricMeterBillDto dto : dtoList) {
              ElectricMeterBill model = new ElectricMeterBill();
              ElectricMeterBillDto billDto = new ElectricMeterBillDto();
              model.setId(dto.getId());
              ElectricMeterBill resultModel = bd.getElectricMeterBillbyId(model);
              billDto.setId(resultModel.getId());
              billDto.setDeviceId(resultModel.getDeviceId());
              billDto.setReadingDate(resultModel.getReadingDate());
              billDto.setReading(resultModel.getReading());
              billDto.setAmount(resultModel.getAmount());
              billDto.setStatus(resultModel.isStatus());
              listDto.getEntries().add(billDto);
          }
      }catch(Exception e) {
          
      }
      System.out.println("GetBillsOfDevice end");
      return listDto;
  }
  
  public HashMap<String, Object> insertBill(ElectricMeterBillDto emb){
      System.out.println("BillService.insertBill " + "start, end");
      HashMap<String, Object> map = new HashMap<String, Object>();
      ElectricMeterBill bill = new ElectricMeterBill();
      bill.setId(emb.getId());
      bill.setReadingDate(emb.getReadingDate());
      bill.setReading(emb.getReading());
      bill.setStatus(emb.isStatus());
      bill.setDeviceId(emb.getDeviceId());
      bill.setOwnerId(emb.getOwnerId());
      try {
          // check if that the item already exists in the datastore
          ElectricMeterBill resultModel = this.bd.getElectricMeterBillbyId(bill);
         // item doesn't exists
             try {
                 resultModel = this.bd.insertElectricMeterBill(bill);
                 emb.setId(resultModel.getId());
                 
                 map.put("id", resultModel.getId());
                 
                 System.out.println("-------Inserting bill was successful.-------");
             } catch (Exception e) {
                 System.out.println("Exception in inserting bill: " + e.toString());
             }
      } catch (Exception e) {
          System.out.println("Exception in inserting bill: " + e.toString());
      }
      return map;
  }
  
  public HashMap<String, Object> getUnpaidBill(List<Long> billIdList) {
      System.out.println("BillService.getUnpaidBill " + "start");
      HashMap<String, Object> map = new HashMap<String, Object>();
      for(Long billId : billIdList) {
          ElectricMeterBill bill = new ElectricMeterBill();
          ElectricMeterBill resultModel = null;
          bill.setId(billId);
          resultModel = bd.getElectricMeterBillbyId(bill);
          if(!resultModel.isStatus()) {
              map.put("id", resultModel.getId());
              map.put("reading", resultModel.getReading());
              map.put("readingDate", resultModel.getReadingDate());
              map.put("amount", resultModel.getAmount());
              break;
          }
      }
      System.out.println("BillService.getUnpaidBill " + "end");
      return map;
  }
  
  public void updateBill(ElectricMeterBillDto emb){
      System.out.println("BillService.updateBill " + "start");
      
      ElectricMeterBill bill = new ElectricMeterBill();
      bill.setId(emb.getId());
      System.out.println(bill.getId() + "id");
      try {
         ElectricMeterBill resultModel = this.bd.getElectricMeterBillbyId(bill);
         resultModel.setStatus(emb.isStatus());
         bd.updateElectricMeterBill(resultModel);
         System.out.println(this.bd.getElectricMeterBillbyId(bill));

      } catch (Exception e) {
          System.out.println("Exception in Updating bill: " + e.toString());
      }
      System.out.println("BillService.updateBill " + "end");
  }
  
  public void updateBillDetails(ElectricMeterBillDto emb){
      System.out.println("BillService.updateBill " + "start");
      
      ElectricMeterBill bill = new ElectricMeterBill();
      bill.setId(emb.getId());
      System.out.println(bill.getId() + "id");
      try {
         ElectricMeterBill resultModel = this.bd.getElectricMeterBillbyId(bill);
         resultModel.setReading(emb.getReading());
         resultModel.setReadingDate(emb.getReadingDate());
         bd.updateElectricMeterBill(resultModel);
         System.out.println(this.bd.getElectricMeterBillbyId(bill));

      } catch (Exception e) {
          System.out.println("Exception in Updating bill: " + e.toString());
      }
      System.out.println("BillService.updateBill " + "end");
  }
  
  public void deleteBills(List<ElectricMeterBillDto> listDto) {
      System.out.println("BillService.deleteBills " + "start");
      for(ElectricMeterBillDto dto : listDto) {
          ElectricMeterBill model = new ElectricMeterBill();
          model.setId(dto.getId());
          System.out.println(model.getId());
          ElectricMeterBill resultModel = bd.getElectricMeterBillbyId(model);
          bd.deleteElectricMeterBill(resultModel);
      }
      System.out.println("BillService.deleteBills " + "end");
   }
}
