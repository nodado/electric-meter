package eletricmeter.service;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import electricmeter.dto.CustomerDto;
import electricmeter.dto.CustomerDto;
import electricmeter.dto.CustomerListDto;
import electricmeter.dto.CustomerDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.dao.CustomerDao;
import eletricmeter.model.Admin;
import eletricmeter.model.Customer;
import eletricmeter.model.Customer;
import eletricmeter.model.Customer;

public class CustomerService {
    
    CustomerDao cDao = new CustomerDao();
    
    public CustomerDto insertCustomer(CustomerDto cDto){
        System.out.println("CustomerService.insertCustomer " + "start, end");
        Customer myCustomer = new Customer();
        Customer customer = new Customer();
        customer.setLastName(cDto.getLastName());
        customer.setFirstName(cDto.getFirstName());
        customer.setAddress(cDto.getAddress());
        customer.setBirthday(cDto.getBirthday());
        customer.setGender(cDto.getGender());
        customer.setUsername(cDto.getUsername());
        customer.setPassword(cDto.getPassword());
        customer.setEmail(cDto.getEmail());
        customer.setMyDevices(cDto.getMyDevices());

        try {
            // check if that the item already exists in the datastore
           Customer resultModel = this.cDao.getCustomerbyId(customer);
           // item doesn't exists
           if (null != resultModel) {
               // Item must not be inserted
               cDto.addError("Item already exists");
           } else {
               // add the item to the datastore
               try {
                   myCustomer = this.cDao.insertCustomer(customer);
                   cDto.setId(myCustomer.getId());
                   System.out.println("id ni nako: "+ myCustomer.getId());
                   System.out.println("-------Inserting customer was successful.-------");
               } catch (Exception e) {
                   cDto.addError("Exception in inserting customer: " + e.toString());
               }
           }
        } catch (Exception e) {
            cDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
        }
        System.out.println("cDto nako: " + cDto.toString());
        return cDto;
    }
    
    public HashMap<String, Object> searchCustomer(CustomerDto cdto){
        System.out.println("Customer.searchCustomer " + "start, end");
        HashMap<String, Object> customerData = new HashMap<String, Object>();
        Customer customer = new Customer();
        
        customer.setUsername(cdto.getUsername());
        customer.setPassword(cdto.getPassword());
        System.out.println("customer : "+ customer.toString());
        try {
            // check if that the item already exists in the datastore
            Customer resultModel = this.cDao.getAdminbyUserAndPassword(customer);
           // item doesn't exists
           if (null == resultModel) {
               // Item must not be inserted
               cdto.addError("User doesn't exist");
           } else if(!resultModel.getPassword().equals(customer.getPassword())){
               // add the item to the datastore
               cdto.addError("Invalid username or password");
           }
           else{
               customerData.put("email", resultModel.getEmail());
               customerData.put("firstName", resultModel.getFirstName());   
               customerData.put("id", resultModel.getId());   
               customerData.put("lastName", resultModel.getLastName());   
               customerData.put("devices", resultModel.getMyDevices());
           }
               
        } catch (Exception e) {
            cdto.addError(GlobalConstants.ERR_DB_EXCEPTION);
            System.out.println(e.getMessage());
        }
        
         return customerData;
    }
    
    public CustomerDto updateCustomer(CustomerDto cDto){
        System.out.println("CustomerService.updateCustomer " + "start, end");
        
        Customer customer = new Customer();
        customer.setId(cDto.getId());
        
        try {
            // check if that the item already exists in the datastore
           Customer resultModel = this.cDao.getCustomerbyId(customer);
           // item doesn't exists
           if (null == resultModel) {
               // Item must not be inserted
               cDto.addError("Item does not exist");
           } else {
               try {
                   resultModel.setMyDevices(cDto.getMyDevices());
                   this.cDao.updateCustomer(resultModel);
                   System.out.println("-------Updating Customer was successful.-------");
               } catch (Exception e) {
                   System.out.println("Exception in updating Customer: " + e.toString());
               }
               }
               
        } catch (Exception e) {
            cDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
        }
        return cDto;
    }
    
    public CustomerListDto getCustomerList() {
        List<Customer> customerList = cDao.getCustomerList();
        return this.getCustomerList(customerList);
    }
    
    public CustomerListDto getCustomerList(List<Customer> aList) {
        CustomerListDto customerListDto = new CustomerListDto();

        try {
            if (aList != null) {
                for (Customer customer : aList) {
                    CustomerDto cDto = new CustomerDto();

                    cDto.setId(customer.getId());
                    cDto.setLastName(customer.getLastName());
                    cDto.setFirstName(customer.getFirstName());
                    cDto.setAddress(customer.getAddress());
                    cDto.setBirthday(customer.getBirthday());
                    cDto.setGender(customer.getGender());
                    cDto.setUsername(customer.getUsername());
                    cDto.setPassword(customer.getPassword());
                    cDto.setEmail(customer.getEmail());
                    cDto.setMyDevices(customer.getMyDevices());
                    
                    customerListDto.getEntries().add(cDto);
                }
            }
        } catch (Exception e) {
            customerListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }

        return customerListDto;
    }
    
    public HashMap<String, Object> getCustomerDevicesIds(CustomerDto dto) {
        HashMap<String, Object>  map = new HashMap<String, Object>();
        try {
            Customer model = new Customer();
            Customer resultModel = null;
            model.setId(dto.getId());
            resultModel = cDao.getCustomerbyId(model);
            map.put("devicesIds",resultModel.getMyDevices());
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return map;
    }

    public HashMap<String, Object> getCustomer(CustomerDto cDto) {
        System.out.println("CustomerService.getCustomer " + "start, end");
        HashMap<String, Object> customerData = new HashMap<String, Object>();

        Customer customer = new Customer();
        customer.setId(cDto.getId());
       
        try {
            // check if that the item already exists in the datastore
            Customer resultModel = cDao.getCustomerbyId(customer);
           // item doesn't exists
           if (null == resultModel) {
               // Item must not be inserted
               cDto.addError("Item does not exist");
           } else {
            // add the item to the datastore
               
               try {
                   customerData.put("id", resultModel.getId());
                   customerData.put("firstName", resultModel.getFirstName());
                   customerData.put("lastName", resultModel.getLastName());
                   customerData.put("email", resultModel.getEmail());
                   customerData.put("username", resultModel.getUsername());
                   customerData.put("password", resultModel.getPassword());
                   customerData.put("gender", resultModel.getGender());
                   customerData.put("birthday", resultModel.getBirthday());
                   customerData.put("address", resultModel.getAddress());
                   customerData.put("myDevices", resultModel.getMyDevices());
                   System.out.println("-------Retrieving Customer was successful.-------");
               } catch (Exception e) {
                   System.out.println("Exception in retrieving Customer: " + e.toString());
               }
           }
               
        } catch (Exception e) {
            cDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
        }
        customerData.put("errorList", cDto.getErrorList());
        return customerData;
    }
    
    public HashMap<String, Object> dunno(CustomerDto cdto){
        System.out.println("Customer.dunno " + "start, end");
        HashMap<String, Object> customerData = new HashMap<String, Object>();
        Customer customer = new Customer();
        System.out.println("new customer: " + customer.toString());
        customer.setUsername(cdto.getUsername());
        customer.setPassword(cdto.getPassword());
        System.out.println("customer : "+ customer.toString());
        try {
            // check if that the item already exists in the datastore
            Customer resultModel = this.cDao.getCustomerbyId(customer);
            System.out.println("resultmodel id: " + resultModel.getId());
           // item doesn't exists
           if (null == resultModel) {
               // Item must not be inserted
               cdto.addError("User doesn't exist");
           } else if(!resultModel.getPassword().equals(customer.getPassword())){
               // add the item to the datastore
               cdto.addError("Invalid username or password");
           }
           else{
               System.out.println("result model: "+ resultModel.toString());
               
               customerData.put("email", resultModel.getEmail());
               customerData.put("firstName", resultModel.getFirstName());   
               customerData.put("id", resultModel.getId());   
               customerData.put("lastName", resultModel.getLastName());   
               customerData.put("devices", resultModel.getMyDevices());
           }
               
        } catch (Exception e) {
            cdto.addError(GlobalConstants.ERR_DB_EXCEPTION);
            System.out.println(e.getMessage());
        }
        
         return customerData;
    }
}
