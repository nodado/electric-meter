package eletricmeter.service;

import java.util.HashMap;
import java.util.List;

import electricmeter.dto.ElectricMeterDeviceDto;
import electricmeter.dto.ElectricMeterDeviceListDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.dao.ElectricMeterDeviceDao;
import eletricmeter.model.ElectricMeterDevice;

public class ElectricMeterDeviceService {
  ElectricMeterDeviceDao dd = new ElectricMeterDeviceDao();
     
  public void insertDevice(ElectricMeterDeviceDto emd){
      System.out.println("DeviceService.insertDevice " + "start, end");
      
      ElectricMeterDevice device = new ElectricMeterDevice();
      device.setMeterNumber(emd.getMeterNumber());
          
      try {
          // check if that the item already exists in the datastore
          ElectricMeterDevice resultModel = this.dd.getElectricMeterDevicebyId(device);
         // item doesn't exists
         if (null != resultModel) {
             // Item must not be inserted
             System.out.println("There is already an item with same name");
         } else {
             // add the item to the datastore
                 device.setPendingBill(emd.getPendingBill());
                 this.dd.insertElectricMeterDevice(device);
                 System.out.println("-------Inserting device was successful.-------");
             } 
      } catch (Exception e) {
          System.out.println("Exception in inserting device: " + e.toString());
      }
  
  }
  
  public HashMap<String, Object> getDevice(ElectricMeterDeviceDto dto) {
      HashMap<String, Object> map = new HashMap<String, Object>();
      try {
          ElectricMeterDevice model = new ElectricMeterDevice();
          ElectricMeterDevice resultModel = null;
          model.setId(dto.getId());
          
          resultModel = dd.getDevicerById(model);
          
          if(resultModel != null) {      
              map.put("id", resultModel.getId());
              map.put("meterNumber", resultModel.getMeterNumber());
              map.put("installationDate", resultModel.getInstallationDate());
              map.put("ownderId", resultModel.getCustomerId());
              map.put("billsIds", resultModel.getBillId());
              map.put("pendingBill", resultModel.getPendingBill());
          }else {
              dto.addError("device not found.");
              map.put("error", dto.getErrorList());
          }
      }catch(Exception e) {
          dto.addError("admin not found.");
          dto.addError(e.getMessage());
          map.put("error", dto.getErrorList());
      }
      return map;
  }
  
  public ElectricMeterDeviceListDto getDevicesByInstallationDate(ElectricMeterDeviceDto from, ElectricMeterDeviceDto to) {
      ElectricMeterDeviceListDto listDto = new ElectricMeterDeviceListDto();
      try {
          ElectricMeterDevice fromModel = new ElectricMeterDevice();
          ElectricMeterDevice toModel = new ElectricMeterDevice();
          fromModel.setInstallationDate(from.getInstallationDate());
          toModel.setInstallationDate(to.getInstallationDate());
          List<ElectricMeterDevice> models = dd.getDevicesByInstallationDate(fromModel, toModel);
          System.out.println(models);
      }catch(Exception e) {
          
      }
      return null;
  }
  
  public ElectricMeterDeviceListDto getDeviceList(){
      List<ElectricMeterDevice> deviceList = dd.getDeviceList();
      return this.getDeviceList(deviceList);
  }

  public ElectricMeterDeviceListDto getDeviceListByCustomerId(long id){
      ElectricMeterDevice device = new ElectricMeterDevice();
      device.setCustomerId(id);
      List<ElectricMeterDevice> deviceList = dd.getDeviceListByUserId(device);
      return this.getDeviceList(deviceList);
  }
  
  public ElectricMeterDeviceListDto getDeviceList(List<ElectricMeterDevice> deviceList){
      ElectricMeterDeviceListDto deviceListDto = new ElectricMeterDeviceListDto();
      try {
          if (deviceList != null) {
              for (ElectricMeterDevice device : deviceList) {
                  ElectricMeterDeviceDto deviceDto = new ElectricMeterDeviceDto();

                  deviceDto.setCustomerId(device.getCustomerId());
                  deviceDto.setId(device.getId());
                  deviceDto.setInstallationDate(device.getInstallationDate());
                  deviceDto.setMeterNumber(device.getMeterNumber());
                  deviceDto.setBillId(device.getBillId());
                  deviceDto.setPendingBill(device.getPendingBill());
                  
                  deviceListDto.getEntries().add(deviceDto);
              }
          }
      } catch (Exception e) {
          deviceListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
      }

      return deviceListDto;
  }
  
  public ElectricMeterDeviceListDto getDevicesByStatus(ElectricMeterDeviceDto dto) {
      ElectricMeterDeviceListDto listDto = new ElectricMeterDeviceListDto();
      try{
          ElectricMeterDevice model = new ElectricMeterDevice();
          model.setPendingBill(dto.getPendingBill());
          List<ElectricMeterDevice> modelList = dd.getDevicesByStatus(model);
          for(ElectricMeterDevice m :modelList) {
              ElectricMeterDeviceDto deviceDto = new ElectricMeterDeviceDto();
              dto.setId(m.getId());
              dto.setInstallationDate(m.getInstallationDate());
              dto.setPendingBill(m.getPendingBill());
              dto.setMeterNumber(m.getMeterNumber());
              dto.setBillId(m.getBillId());
              
              listDto.getEntries().add(dto);
          }
      }catch(Exception e) {
          System.out.println("Exception:" + e.getMessage());
      }
      return listDto;
  }
  
  public ElectricMeterDeviceListDto getCustomerDevices(List<ElectricMeterDeviceDto> deviceList) {
      ElectricMeterDeviceListDto deviceListDto = new ElectricMeterDeviceListDto();
      try {
          for(ElectricMeterDeviceDto e : deviceList) {
              ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
              ElectricMeterDevice model= new ElectricMeterDevice();
              ElectricMeterDevice resultModel = null;
              
              model.setId(e.getId());
              resultModel = dd.getElectricMeterDevicebyId(model);
              
              dto.setId(resultModel.getId());
              dto.setCustomerId(resultModel.getCustomerId());
              dto.setMeterNumber(resultModel.getMeterNumber());
              dto.setBillId(resultModel.getBillId());
              dto.setInstallationDate(resultModel.getInstallationDate());
              dto.setPendingBill(resultModel.getPendingBill());
              
              deviceListDto.addEntry(dto);
          }
      } catch (Exception e) {
          deviceListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
      }

      return deviceListDto;
  }
  
  public ElectricMeterDeviceListDto getDevicesPendingBill(List<ElectricMeterDeviceDto> deviceList) {
      ElectricMeterDeviceListDto deviceListDto = new ElectricMeterDeviceListDto();
      try {
          for(ElectricMeterDeviceDto e : deviceList) {
              ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
              ElectricMeterDevice model= new ElectricMeterDevice();
              ElectricMeterDevice resultModel = null;
              
              model.setId(e.getId());
              resultModel = dd.getElectricMeterDevicebyId(model);
              if(resultModel.getPendingBill()) {
                  dto.setId(resultModel.getId());
                  dto.setCustomerId(resultModel.getCustomerId());
                  dto.setMeterNumber(resultModel.getMeterNumber());
                  dto.setBillId(resultModel.getBillId());
                  dto.setInstallationDate(resultModel.getInstallationDate());
                  dto.setPendingBill(resultModel.getPendingBill());
                  
                  deviceListDto.addEntry(dto); 
              }
          }
      } catch (Exception e) {
          deviceListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
      }

      return deviceListDto;
  }
  
  public void updateDevice(ElectricMeterDeviceDto emd){
      System.out.println("DeviceService.updateDevice " + "start, end");
      
      ElectricMeterDevice device = new ElectricMeterDevice();
      device.setId(emd.getId());     
      try {
          // check if that the item already exists in the datastore
         ElectricMeterDevice resultModel = this.dd.getElectricMeterDevicebyId(device);
         
         resultModel.setInstallationDate(emd.getInstallationDate());
         resultModel.setBillId(emd.getBillId());
         resultModel.setPendingBill(emd.getPendingBill());
         dd.updateElectricMeterDevice(resultModel);
             
      } catch (Exception e) {
          System.out.println("Exception in Updating device: " + e.toString());
      }
  }
  
  public void deleteDevice(ElectricMeterDeviceDto emd){
      System.out.println("DeviceService.deleteDevice " + "start");
      
      ElectricMeterDevice device = new ElectricMeterDevice();
      device.setId(emd.getId());
      device.setInstallationDate(emd.getInstallationDate());
      device.setMeterNumber(emd.getMeterNumber());
      
      try {
          
          ElectricMeterDevice resultModel = this.dd.getElectricMeterDevicebyId(device);
          
          if (null != resultModel) {
              this.dd.deleteElectricMeterDevice(resultModel);
          } 
      } catch (Exception e) {
          System.out.println("Exception in deleting device: " + e.toString());
      }
      System.out.println("device.deletedevice " + "end");
  }
  
  public void deleteBillsOfDevice(ElectricMeterDeviceDto dto) {
      System.out.println("DeviceService.deleteBillsOfDevice " + "start");
      try{
          ElectricMeterDevice model = new ElectricMeterDevice();
          model.setId(dto.getId());
          ElectricMeterDevice resultModel = dd.getDevicerById(model);
          resultModel.setBillId(dto.getBillId());
          dd.updateElectricMeterDevice(resultModel);
      }catch(Exception e) {
          System.out.println("Exception: " + e.getMessage());
      }
      System.out.println("DeviceService.deleteBillsOfDevice " + "end");
  }

  public ElectricMeterDeviceListDto getDevices() {
      ElectricMeterDeviceListDto listDto = new ElectricMeterDeviceListDto();
      try {
          List<ElectricMeterDevice> listModel = dd.getDevices();
          
          for(ElectricMeterDevice model : listModel) {
              ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
           
              dto.setId(model.getId());
              dto.setMeterNumber(model.getMeterNumber());
              dto.setInstallationDate(model.getInstallationDate());
              listDto.addEntry(dto);
          }
      }catch(Exception e) {
         listDto.addError(e.getMessage());
      }
      return listDto;
  }
  
  public ElectricMeterDeviceListDto getDevicesNoOwner() {
      ElectricMeterDeviceListDto listDto = new ElectricMeterDeviceListDto();
      try {
          List<ElectricMeterDevice> listModel = dd.getDevicesNoOwner();
          
          for(ElectricMeterDevice model : listModel) {
              ElectricMeterDeviceDto dto = new ElectricMeterDeviceDto();
           
              dto.setId(model.getId());
              dto.setMeterNumber(model.getMeterNumber());
              dto.setInstallationDate(model.getInstallationDate());
              listDto.addEntry(dto);
          }
      }catch(Exception e) {
         listDto.addError(e.getMessage());
      }
      return listDto;
  }
  
  public ElectricMeterDeviceListDto getCustomerDeviceByStatus(ElectricMeterDeviceDto dto) {
      ElectricMeterDeviceListDto listDto = new ElectricMeterDeviceListDto();
      try {
         ElectricMeterDevice model = new ElectricMeterDevice();
         model.setCustomerId(dto.getCustomerId());
         model.setPendingBill(dto.getPendingBill());
         List<ElectricMeterDevice> models = dd.getCustomerDevicesByStatus(model);
         for(ElectricMeterDevice m : models) {
             ElectricMeterDeviceDto deviceDto = new ElectricMeterDeviceDto();
             deviceDto.setBillId(m.getBillId());
             deviceDto.setId(m.getId());
             deviceDto.setInstallationDate(m.getInstallationDate());
             deviceDto.setMeterNumber(m.getMeterNumber());
             deviceDto.setPendingBill(m.getPendingBill());
             listDto.addEntry(deviceDto);
         }
      }catch(Exception e) {
          System.out.println("Exception: " + e.getMessage());
      }
      return listDto;
  }
   
  public void registerDevicesToOwner(List<ElectricMeterDeviceDto> listDto) {
     
      try {
          for(ElectricMeterDeviceDto e : listDto) {
              ElectricMeterDevice model = new ElectricMeterDevice();
              model.setId(e.getId());        
              ElectricMeterDevice resultModel = dd.getDevicerById(model);
              resultModel.setCustomerId(e.getCustomerId());
              resultModel.setInstallationDate(e.getInstallationDate());
              dd.updateElectricMeterDevice(resultModel);
          }
      }catch(Exception e) {
          System.out.println(e.getMessage() + "exception");
      }
      System.out.println("update success");
  }
  
  public void registerBilsToDevice(ElectricMeterDeviceDto dto) {
      System.out.println("DeviceService.RegisterBillToDevice " + "start");
      
      ElectricMeterDevice device = new ElectricMeterDevice();
      device.setId(dto.getId());     
      try {
          // check if that the item already exists in the datastore
         ElectricMeterDevice resultModel = this.dd.getElectricMeterDevicebyId(device);
         
         resultModel.setBillId(dto.getBillId());
         resultModel.setPendingBill(dto.getPendingBill());
         dd.updateElectricMeterDevice(resultModel);
             
      } catch (Exception e) {
          System.out.println("Exception in Updating device: " + e.toString());
      }
      System.out.println("DeviceService.RegisterBillToDevice" + "end");
  }
  
  public void updateElectricMeterPendingBill(ElectricMeterDeviceDto dto) {
      System.out.println("Update Electric Meter Pending Bill start");
      try {
          ElectricMeterDevice model = new ElectricMeterDevice();
          ElectricMeterDevice resultModel = null;
          
          model.setId(dto.getId());
          resultModel = dd.getElectricMeterDevicebyId(model);
          resultModel.setPendingBill(dto.getPendingBill());
          dd.updateElectricMeterDevice(resultModel);
          
      }catch(Exception e) {
          System.out.println("Exception:" + e.getMessage());
      }
      System.out.println("Update Electric Meter Pending Bill end");
  }
  
  public void updateDeviceDate(ElectricMeterDeviceDto dto) {
      try {
          ElectricMeterDevice device = new ElectricMeterDevice();
          device.setId(dto.getId());   
          ElectricMeterDevice resultModel = this.dd.getElectricMeterDevicebyId(device);
          resultModel.setInstallationDate(dto.getInstallationDate());
          dd.updateElectricMeterDevice(resultModel);
      }catch(Exception e) {
          System.out.println("Exception:" + e.getMessage());
      }
  }
  
}
