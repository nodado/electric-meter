package eletricmeter.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.appengine.api.datastore.Entity;

import electricmeter.dto.AdminDto;
import electricmeter.utils.GlobalConstants;
import eletricmeter.dao.AdminDao;
import eletricmeter.model.Admin;
import electricmeter.dto.AdminListDto;

public class AdminService {
    
    private AdminDao aDao = new AdminDao();

    public AdminDto insertAdmin(AdminDto aDto){
    System.out.println("AdminService.insertAdmin " + "start, end");

    Admin admin = new Admin();
    admin.setLastName(aDto.getLastName());
    admin.setFirstName(aDto.getFirstName());
    admin.setEmail(aDto.getEmail());
    admin.setUsername(aDto.getUsername());
    admin.setPassword(aDto.getPassword());
    admin.setGender(aDto.getGender());
    admin.setBirthday(aDto.getBirthday());
    admin.setRights(aDto.getRights());

    try {
        // check if that the item already exists in the datastore
        Admin resultModel = this.aDao.getAdminbyName(admin);
       // item doesn't exists
       if (null != resultModel) {
           // Item must not be inserted
           aDto.addError("Item already exists");
       } else {
           // add the item to the datastore
           try {
               this.aDao.insertAdmin(admin);
               System.out.println("-------Inserting Admin was successful.-------");
           } catch (Exception e) {
               System.out.println("Exception in inserting Admin: " + e.toString());
           }
       }
    } catch (Exception e) {
        aDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
    }
    return aDto;
    }
    public HashMap<String, Object> searchAdmin(AdminDto aDto){
        System.out.println("AdminService.searchAdmin " + "start, end");
        
        Admin admin = new Admin();
        HashMap<String, Object> map = new  HashMap<String, Object>();
        admin.setUsername(aDto.getUsername());
        admin.setPassword(aDto.getPassword());
        
        try {
            // check if that the item already exists in the datastore
            Admin resultModel = this.aDao.getAdminbyUserAndPassword(admin);
           // item doesn't exists
           if (null == resultModel) {
               // Item must not be inserted
               aDto.addError("User doesn't exist");
               map.put("erroList", aDto.getErrorList());
           } else if(!resultModel.getPassword().equals(admin.getPassword())){
               // add the item to the datastore
               aDto.addError("Invalid username or password");
               map.put("erroList", aDto.getErrorList());
           }
           else{
               aDto.setEmail(resultModel.getEmail());
               aDto.setFirstName(resultModel.getFirstName());
               aDto.setId(resultModel.getId());
               aDto.setLastName(resultModel.getLastName());
               aDto.setRights(resultModel.getRights());
               
               map.put("username", resultModel.getUsername());
               map.put("firstName", resultModel.getFirstName());
               map.put("lastName", resultModel.getLastName());
               map.put("email", resultModel.getEmail());
           }
               
        } catch (Exception e) {
            aDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
            map.put("erroList", aDto.getErrorList());
        }
         return map;
    }
    
    public AdminDto updateAdmin(AdminDto aDto){
    System.out.println("AdminService.updateAdmin " + "start, end");

    Admin admin = new Admin();
    admin.setId(aDto.getId());
    admin.setLastName(aDto.getLastName());
    admin.setFirstName(aDto.getFirstName());
    admin.setEmail(aDto.getEmail());
    admin.setGender(aDto.getGender());
    admin.setBirthday(aDto.getBirthday());
    admin.setRights(aDto.getRights());
   
    try {
        // check if that the item already exists in the datastore
        Admin resultModel = this.aDao.getAdminbyId(admin);
       // item doesn't exists
       if (null == resultModel) {
           // Item must not be inserted
           aDto.addError("Item does not exist");
       } else {
        // add the item to the datastore
           try {
               admin.setKey(resultModel.getKey());
               this.aDao.updateAdmin(admin);
               System.out.println("-------Updating Admin was successful.-------");
           } catch (Exception e) {
               System.out.println("Exception in updating Admin: " + e.toString());
           }
       }
           
    } catch (Exception e) {
        aDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
    }
    
    return aDto;
    }
    
    public AdminDto deleteAdmin(AdminDto aDto){
    System.out.println("AdminService.deleteCustomer " + "start");

    Admin admin = new Admin();
    admin.setId(aDto.getId());

    try {
       
        Admin resultModel = this.aDao.getAdminbyId(admin);
        
        if (null == resultModel) {
            // Item must not be inserted
            aDto.addError("Item does not exist");
        } else {
            // add the item to the datastore
            try {
                this.aDao.deleteAdmin(resultModel);
                System.out.println("-------Deleting Admin was successful.-------");
            } catch (Exception e) {
                System.out.println("Exception in deleting Admin: " + e.toString());
            }
        }
    } catch (Exception e) {
        aDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
    }

    System.out.println("Admin.deleteAdmin " + "end");
    return aDto;
    }
    
    public HashMap<String, Object> getAdmin(AdminDto aDto) {
        System.out.println("AdminService.getAdmin " + "start, end");
        HashMap<String, Object> adminData = new HashMap<String, Object>();

        Admin admin = new Admin();
        admin.setId(aDto.getId());
       
        try {
            // check if that the item already exists in the datastore
            Admin resultModel = this.aDao.getAdminbyId(admin);
           // item doesn't exists
           if (null == resultModel) {
               // Item must not be inserted
               aDto.addError("Item does not exist");
           } else {
            // add the item to the datastore
               try {
                   adminData.put("id", resultModel.getId());
                   adminData.put("firstName", resultModel.getFirstName());
                   adminData.put("lastName", resultModel.getLastName());
                   adminData.put("email", resultModel.getEmail());
                   adminData.put("username", resultModel.getUsername());
                   adminData.put("password", resultModel.getPassword());
                   adminData.put("gender", resultModel.getGender());
                   adminData.put("birthday", resultModel.getBirthday());
                   adminData.put("rights", resultModel.getRights());
                   System.out.println("-------Retrieving Admin was successful.-------");
               } catch (Exception e) {
                   System.out.println("Exception in retrieving Admin: " + e.toString());
               }
           }
               
        } catch (Exception e) {
            aDto.addError(GlobalConstants.ERR_DB_EXCEPTION);
        }
        adminData.put("errorList", aDto.getErrorList());
        return adminData;
    }
    
    public AdminListDto getAdminList() {
        List<Admin> adminList = aDao.getAdminList();
        System.out.println(adminList.get(0).getFirstName());
        return this.getAdminList(adminList);
    }
    
    public AdminListDto getAdminList(List<Admin> aList) {
        AdminListDto adminListDto = new AdminListDto();

        try {
            if (aList != null) {
                for (Admin admin : aList) {
                    AdminDto aDto = new AdminDto();

                    aDto.setId(admin.getId());
                    aDto.setLastName(admin.getLastName());
                    aDto.setFirstName(admin.getFirstName());
                    aDto.setEmail(admin.getEmail());
                    aDto.setUsername(admin.getUsername());
                    aDto.setPassword(admin.getPassword());
                    aDto.setGender(admin.getGender());
                    aDto.setBirthday(admin.getBirthday());
                    aDto.setRights(admin.getRights());
                    
                    adminListDto.getEntries().add(aDto);
                }
            }
        } catch (Exception e) {
            adminListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }

        return adminListDto;
    }

}
