package electricmeter.dto;

import java.util.ArrayList;
import java.util.Date;

public class AdminDto extends BaseDto{

private Long id;
    
    private String lastName;
    
    private String firstName;
    
    private String username;

    private String password;
    
    private String gender;

    private Date birthday;
    
    private String email;
    
    private ArrayList<Boolean> rights  = new ArrayList<Boolean>(4);

    public ArrayList<Boolean> getRights() {
        return rights;
    }

    public void setRights(ArrayList<Boolean> rights) {
        this.rights = rights;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
