package electricmeter.dto;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ElectricMeterDeviceDto extends BaseDto{

    private String meterNumber;
    
    private Date installationDate;
    
    private Date creationDate;
    
    private Date modificaitonDate;
    
    private Long id;
    
    private long customerId;
    
    private List<Long> billId = new LinkedList<Long>();
      
    private Boolean pendingBill;
    
    public Boolean getPendingBill() {
        return pendingBill;
    }

    public void setPendingBill(Boolean pendingBill) {
        this.pendingBill = pendingBill;
    }
    
    public List<Long> getBillId() {
        return billId;
    }

    public void setBillId(List<Long> billId) {
        this.billId = billId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificaitonDate() {
        return modificaitonDate;
    }

    public void setModificaitonDate(Date modificaitonDate) {
        this.modificaitonDate = modificaitonDate;
    }
}
