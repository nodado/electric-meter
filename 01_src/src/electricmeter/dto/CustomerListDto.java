package electricmeter.dto;

import java.util.ArrayList;
import java.util.List;

public class CustomerListDto extends BaseDto{

    private List<CustomerDto> customers = new ArrayList<CustomerDto>();

    public List<CustomerDto> getEntries() {
        return customers;
    }

    public void setEntries(List<CustomerDto> customers) {
        this.customers = customers;
    }
}
