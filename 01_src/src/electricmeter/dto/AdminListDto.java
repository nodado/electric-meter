package electricmeter.dto;

import java.util.ArrayList;
import java.util.List;

public class AdminListDto extends BaseDto{

    private List<AdminDto> admins = new ArrayList<AdminDto>();

    public List<AdminDto> getEntries() {
        return admins;
    }

    public void setEntries(List<AdminDto> admins) {
        this.admins = admins;
    }
}
