package electricmeter.dto;

import java.util.ArrayList;
import java.util.Date;

public class CustomerDto extends BaseDto{

    //id, firstName, lastName, email, status, gender, birthday, address, account
    
    private Long id;
    
    private String lastName;
    
    private String firstName;
    
    private String address;
    
    private String email;

    private Date birthday;
    
    private String gender;
    
    private String username;
    
    private String password;
    
    private ArrayList<Long> myDevices;
    
    private ArrayList<Long> deletedBills;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;   
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Long> getMyDevices() {
        return myDevices;
    }

    public void setMyDevices(ArrayList<Long> myDevices) {
        this.myDevices = myDevices;
    }

    public ArrayList<Long> getDeletedBills() {
        return deletedBills;
    }

    public void setDeletedBills(ArrayList<Long> deletedBills) {
        this.deletedBills = deletedBills;
    }

    @Override
    public String toString() {
        return "CustomerDto [id="
            + id
            + ", lastName="
            + lastName
            + ", firstName="
            + firstName
            + ", address="
            + address
            + ", email="
            + email
            + ", birthday="
            + birthday
            + ", gender="
            + gender
            + ", username="
            + username
            + ", password="
            + password
            + ", myDevices="
            + myDevices
            + ", deletedBills="
            + deletedBills
            + "]";
    }
    
    
}
