package electricmeter.dto;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;



public class ElectricMeterDeviceListDto extends BaseDto{

    private LinkedList<ElectricMeterDeviceDto> listDto;
    
    public ElectricMeterDeviceListDto() {
        this.listDto = new LinkedList<ElectricMeterDeviceDto>();
    }
    
    public void addEntry(ElectricMeterDeviceDto dto) {
        this.listDto.add(dto);
    }
    
    public LinkedList<ElectricMeterDeviceDto> getEntries() {
        return this.listDto;
    }

}
