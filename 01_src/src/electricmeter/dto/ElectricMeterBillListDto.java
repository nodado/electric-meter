package electricmeter.dto;

import java.util.ArrayList;
import java.util.List;

public class ElectricMeterBillListDto extends BaseDto{

    private List<ElectricMeterBillDto> bills = new ArrayList<ElectricMeterBillDto>();

    public List<ElectricMeterBillDto> getEntries() {
        return bills;
    }

    public void setEntries(List<ElectricMeterBillDto> bills) {
        this.bills = bills;
    }

}
